#!/bin/bash

translation=(fr)

# Create .pot files
for lang in "${translation[@]}" ; do
    if [ ! -d "asmx86/i18n/$lang" ] ; then
        mkdir -p "asmx86/i18n/$lang"
    fi

    if [ ! -f "asmx86/i18n/$lang/asmx86.po" ] ; then
        msginit -i "asmx86/i18n/asmx86.pot" -o "asmx86/i18n/$lang/asmx86.po"
    fi
done

# Generate .po files
xgettext -k_ -i --strict -s --omit-header -o "asmx86/i18n/asmx86.pot" \
    --copyright-holder="Kawa Team" --package-name="asmx86" --from-code="utf-8" \
    --package-version="0.1" asmx86/ui/qt/*.py asmx86/ui/qt/widget/*.py \
    asmx86/ui/gtk/*.py asmx86/ui/*.py

for lang in "${translation[@]}" ; do
    msgmerge -s -U "asmx86/i18n/$lang/asmx86.po" "asmx86/i18n/asmx86.pot"

    if [ ! -d asmx86/i18n/$lang/LC_MESSAGES ] ; then
        mkdir -p "asmx86/i18n/$lang/LC_MESSAGES"
    fi

    msgfmt "asmx86/i18n/$lang/asmx86.po" -o "asmx86/i18n/$lang/LC_MESSAGES/asmx86.mo"
done
