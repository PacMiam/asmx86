��    *      l  ;   �      �     �     �     �     �     �     �  	   �  !   �          7  $   =     b     ~     �     �     �     �     �     �  '   �     
          $     '  	   4     >  	   E     O  	   \  	   f     p     w          �  	   �     �     �     �     �     �              1     L     U     ]     u     �  
   �  "   �     �     �  (   �       *   ;     f     r     x     �     �     �  -   �     �     	     "	     &	     ;	  	   M	  	   W	     a	     {	     �	     �	     �	  
   �	     �	  	   �	     �	     �	  #   

  "   .
  !   Q
     s
                                       "             #          (                 !                        *                       )                          $   %            
          	              &   '    %s file not exists Accept Cancel Close %s file Close Untitled file Data Execution External module %s already exists File %s has been modified Flags Global declaration %s already exists Instruction not implemented Kernel call read error Labels Line Main Interface Message Missing data section Missing text section Missing the closing directive for macro Modules New untitled file No Open %s file Open File Parser Registers Save %s file Save File Search... Status Symbols Untitled Untitled.nasm Variables Welcome in ASMx86 Would you want to save it ? Wrong instruction syntax Wrong label syntax Wrong variable syntax Yes Project-Id-Version: asmx 86
PO-Revision-Date: 2018-02-13 22:03+0100
Last-Translator: PacMiam <pacmiam@tuxfamily.org>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Le fichier %s n'existe pas Accepter Annuler Fermeture du fichier %s Fermeture du fichier Sans titre Données Exécution Le module externe %s existe déjà Le fichier %s a été modifié Drapeaux La déclaration globale %s existe déjà Instruction non implémentée Erreur lors de l'appel de lecture du noyau Étiquettes Ligne Interface principale Message La section data est manquante La section text est manquante La balise de fermeture de macro est manquante Modules Nouveau fichier sans titre Non Ouvrir le fichier %s Ouvrir un fichier Analyseur Registres Sauvegarder le fichier %s Sauvegarder un fichier Rechercher... Statut Symboles Sans titre Sans titre.nasm Variables Bienvenue sur ASMx86 Souhaitez-vous le sauvegarder ? Mauvaise syntaxe pour l'instruction Mauvaise syntaxe pour l'étiquette Mauvaise syntaxe pour la variable Oui 