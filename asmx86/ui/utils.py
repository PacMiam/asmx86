# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser
from os.path import join as path_join

# Regex
from re import sub

# System
from os import environ
from sys import exit as sys_exit

# ------------------------------------------------------------------------------
#   Modules - Packages
# ------------------------------------------------------------------------------

try:
    from pkg_resources import resource_filename
    from pkg_resources import DistributionNotFound

except ImportError as error:
    sys_exit("Import error with python3-setuptools module: %s" % str(error))

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.lib.flag import AsmError

# ------------------------------------------------------------------------------
#   Modules - Translation
# ------------------------------------------------------------------------------

from gettext import gettext as _
from gettext import textdomain
from gettext import bindtextdomain

# ------------------------------------------------------------------------------
#   Methods
# ------------------------------------------------------------------------------

def get_data(path, egg="asmx86"):
    """ Provides easy access to data in a python egg or local folder

    This function search a path in a specific python egg or in local folder. The
    local folder is check before egg to allow quick debugging.

    Thanks Deluge :)

    Parameters
    ----------
    path : str
        File path

    Other Parameters
    ----------------
    egg : str
        Python egg name (Default: gem)

    Returns
    -------
    str or None
        Path
    """

    try:
        data = resource_filename(egg, path)

        if exists(expanduser(data)):
            return data

        return path

    except DistributionNotFound as error:
        return path

    except KeyError as error:
        return path

    return None


def replace_for_markup(text):
    """ Replace some characters in text for markup compatibility

    Parameters
    ----------
    text : str
        Text to parser

    Returns
    -------
    str
        Replaced text
    """

    characters = {
        '&': "&amp;",
        '<': "&lt;",
        '>': "&gt;",
    }

    for key, value in characters.items():
        text = text.replace(key, value)

    return text


def get_binary_path(binary):
    """ Get a list of available binary paths from $PATH variable

    This function get all the path from $PATH variable which match binary
    request.

    Parameters
    ----------
    binary : str
        Binary name or path

    Returns
    -------
    list
        List of available path

    Examples
    --------
    >>> get_binary_path("ls")
    ['/bin/ls']
    """

    available = list()

    if len(binary) == 0:
        return available

    if exists(expanduser(binary)):
        available.append(binary)
        binary = basename(binary)

    for path in set(environ["PATH"].split(':')):
        binary_path = expanduser(path_join(path, binary))

        if exists(binary_path) and not binary_path in available:
            available.append(binary_path)

    return available


bindtextdomain("asmx86", get_data("i18n"))
textdomain("asmx86")

def get_parser_messages(position, message, *args):
    """ Retrieve message from parsing process

    Parameters
    ----------
    position : int
        Message position
    message : asmx86.lib.flag.AsmError
        Message type

    Returns
    -------
    str
        Translated message
    """

    if message == AsmError.AlreadyExistsExternal:
        message = _("External module %s already exists") % str(args[0])

    elif message == AsmError.AlreadyExistsGlobal:
        message = _("Global declaration %s already exists") % str(args[0])

    elif message == AsmError.MissingSectionData:
        message = _("Missing data section")

    elif message == AsmError.MissingSectionText:
        message = _("Missing text section")

    elif message == AsmError.MissingCloseMacro:
        message = _("Missing the closing directive for macro")

    elif message == AsmError.WrongLabelSyntax:
        message = _("Wrong label syntax")

    elif message == AsmError.WrongVariableSyntax:
        message = _("Wrong variable syntax")

    elif message == AsmError.WrongInstructionSyntax:
        message = _("Wrong instruction syntax")

    elif message == AsmError.KernelCallReadError:
        message = _("Kernel call read error")

    elif message == AsmError.InstructionNotImplemented:
        message = _("Instruction not implemented")

    else:
        message = str(message)

    return (position, message)
