
# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHAN__tabILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#   Modules - System
#-------------------------------------------------------------------------------

import sys

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser
#-------------------------------------------------------------------------------
#   Modules - PyQT5
#-------------------------------------------------------------------------------

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

#-------------------------------------------------------------------------------
#   Modules - ASMx86
#-------------------------------------------------------------------------------

#syntaxic coloration
from asmx86.ui.qt.widget.asmxSyntax import AsmxSyntax
from asmx86.ui.qt.widget.asmxEditor import AsmxEditor

#num lines and highlight current line
from asmx86.ui.qt.widget.editor import CodeEditor
from asmx86.lib.engine import Engine

#-------------------------------------------------------------------------------
#   Class
#-------------------------------------------------------------------------------

class CustomQtQuestionBox(QMessageBox):
    """Create Qt QMessageBox with custom name for buttons
    """

    def __init__(self, title, text, yesName, noName, yesFunc, noFunc):
        """Constructor
        
        Parameters
        ----------

        title : str
            MessageBox's title as string

        text : str
            MessageBox's text as string

        yesName : str
            yes Message's box button name as string

        noName : str
            no Message's box button name as string
        yesFunc : function
            function to execute in case of "yes" answer
        
        noFunc : function
            function to execute in case of "no" answer
            
        """

        super().__init__()
        
        # ----------------------------------------
        #   Variables
        # ----------------------------------------
        
        self.setIcon(QMessageBox.Question)
        self.setWindowTitle(title)
        self.setText(text)
        self.setStandardButtons(QMessageBox.No | QMessageBox.Yes)
        self.buttonY = self.button(QMessageBox.Yes)
        self.buttonY.setText(yesName)
        self.buttonN = self.button(QMessageBox.No)
        self.buttonN.setText(noName)
        self.__yes_func = yesFunc
        self.__no_func = noFunc
        self.__y_arg = None
        self.__n_arg = None
        
    def setYargs(self, *arg):
        """Define arguments for the "yes" answer function
        
        parameters
        ----------
        arg : arguments
            arguments of the "yes " answer function
        
        """
    
        self.__y_arg = arg
        
        
    def setNargs(self, *arg):
        """Define arguments for the "no" answer function
        
        parameters
        ----------
        arg : arguments
            arguments of the "no " answer function
        
        """
        
        self.__n_arg = arg
        
    def run(self):
        """Run the questionbox 
        """
        
        self.exec()
        
        
        if self.clickedButton() == self.buttonY:
            if self.__yes_func is not None :
                if self.__y_arg is not None :
                    self.__yes_func(*self.__y_arg)
                else :
                    self.__yes_func()
            
        elif self.clickedButton() == self.buttonN:
            if self.__no_func is not None :
                if self.__n_arg is not None :
                    self.__no_func(*self.__n_arg)
                    
                else :
                    self.__no_func()
        

class ToolBarAction(QAction):
    """Create Qt ToolBar Action
    """

    def __init__(self, theme, text, trigger, parrent):
        """Constructor
        Parameters
        ----------

        theme : str
            Icon theme for the action as string

        text : str
            Action's text as string

        trigger : Function
            Action's box function to be trigger as Function

        parrent : Object
            Action's parrent as Object
        
        """

        super().__init__(QIcon.fromTheme(theme), text, parrent)

        self.triggered.connect(trigger)


class MenuAction(QAction):
    """Create Qt Menu Action
    """
    def __init__(self, name, shortcut, statust_tip, trigger, parrent):
        """Constructor
        Parameters
        ----------

        name : str
            Action's name as str

        shortcut : str
            Action's shortcut as string

        statust_tip : str
            Action's status tip as string

        trigger : Function
            Action's box function to be trigger as Function

        parrent : Object
            Action's parrent as Object
        """

        super().__init__(name, parrent)

        self.setShortcut(shortcut)
        self.setStatusTip(statust_tip)
        self.triggered.connect(trigger)
        
        
class HBoxSeparatorGenerator(QObject) :
    """Generator of QTHboxLayout separator
    """
    

    def generate_separator(self):
        """generate a separator
        """
        line = QFrame()
        line.setFrameShape(QFrame.VLine)
        line.setFrameShadow(QFrame.Sunken)
        line.setLineWidth(1);
        
        return line

class AsmxTabEditor(AsmxEditor) :
    """Widget to manage different file as editors in tab
    """
    def __init__(self, theme, file_path, debug) :
        """Constructor
        
        Parameters :
        ------------
        theme : ThemeColors
            the theme colors to apply to the code
        file_path : string  
            the pathe of the file containing the code
        debug : bool
            true if debug mode, else false
        
        """
    
        super().__init__(theme)
        
        self.edit_reg_signal_connected = False
        self.edited = False
        self.symbol_model = None
        self.execute_buffer = ""
        self.engine = None
        
        self.output_model = QStandardItemModel()
        self.output_model.setHorizontalHeaderLabels(['Message'])
        
        self.__debug = debug
        
        self.file_path = file_path
        
        self.create_engine()
    
    def unsaved_edit(self) :
        """Determine if code edited since last save
        
        Returns :
        ---------
        Bool
            true if edited, else false
        """
        
        return self.edited
        
    def create_engine(self):
        """Create the engine
        """

        # Generate a new instance for specified path
        self.engine = Engine(self.file_path, self.__debug)
        self.engine.parse()

        # Retrieve file buffer
        self.current_buffer = '\n'.join(self.engine.get_buffer())

        # Insert file buffer into editor
        self.setPlainText(self.current_buffer)
        # self.setWindowTitle("Asmx86 : "+ self.file_path)

    def refresh_data(self) :
        """Refresh the data in the tab Widgets 

        """
        
        if self.__debug :
            
            if self.engine is not None :
                print("[qt interface] {0} Registers :\n".format(
                    len(self.engine.get_registers())))
                for register in self.engine.get_registers():
                    print("{0} : {1}".format(register.name, register.value))
                print("")

                print("[qt interface] {0} Variables :\n".format(
                    len(self.engine.get_program().get_variables())))
                for variable in self.engine.get_program().get_variables():
                    print("{0} : {1}".format(variable.name, variable.value))
                print("")

                print("[qt interface] {0} Flags :\n".format(
                    len(self.engine.get_flags())))
                for flag in self.engine.get_flags():
                    print("{0} : {1}".format(flag.name, flag.value))
                print("")

            else :

                print("[qt interface] Warning use of \"refresh_data\" with None Engine")
                
                
        if self.engine is not None:
            
            program = self.engine.get_program()

            subprograms = program.get_macros()
            subprograms.insert(0, program)
            
            #-----------------------------------------------------------------
            # Tab Zone Symbols
            #-----------------------------------------------------------------
            
            self.symbol_model = QStandardItemModel()
            self.symbol_model.setHorizontalHeaderLabels(['Name', 'Value', 'Row'])
            
            parent1 = QStandardItem('Variables')
            for element in subprograms:
                for variable in element.get_variables() :

                    child1 = QStandardItem(variable.name)
                    child2 = QStandardItem(variable.value)
                    child3 = QStandardItem(str(variable.position + 1))

                    parent1.appendRow([child1, child2, child3])
            self.symbol_model.appendRow(parent1)
            
            parent1 = QStandardItem('Labels')
            for element in subprograms:
                for section in element.get_sections():
                    for label in section.get_labels():

                        child1 = QStandardItem(label.name)
                        child2 = QStandardItem("")
                        child3 = QStandardItem(str(label.position + 1))

                        parent1.appendRow([child1, child2, child3])

            self.symbol_model.appendRow(parent1)
            
            
            parent1 = QStandardItem('Modules')
            for element in subprograms:
                for module in element.get_externals():
                    
                    child1 = QStandardItem(module.name)
                    child2 = QStandardItem("")
                    child3 = QStandardItem(str(module.position + 1))
                
                    parent1.appendRow([child1, child2, child3])
            self.symbol_model.appendRow(parent1)
            
            
            #-----------------------------------------------------------------
            # Tab Zone Data
            #-----------------------------------------------------------------

            
            self.data_model = QStandardItemModel()
            self.data_model.setHorizontalHeaderLabels(['Name', 'Value'])
            
            
            parent1 = QStandardItem("Registres")
            
            iters = dict()
            for size in self.engine.get_registers_arch():
                iters[size] = QStandardItem("{} bits".format(size))
                
                if self.__debug :
                    print("[Tab Zone Registers] size : {}".format(size))
                

            for register in self.engine.get_registers() :
            
                if self.__debug :
                    print("[Tab Zone Registers] name : {} value : {} size : {}".format(register.name, register.value,(register.size * 8)))
                    
                child1 = QStandardItem(register.name)
                child2 = QStandardItem(str(register.value))
                
                iters[(register.size * 8)].appendRow([child1, child2])
            
            for value in iters.values() :
                parent1.appendRow(value)
            
            self.data_model.appendRow(parent1)
            
            parent1 = QStandardItem("Flags")
            
            for flag in self.engine.get_flags():
                
                if self.__debug :
                    print ("[Tab Zone flag] name : {} value : {}".format(flag.name, flag.value))
                
                child1 = QStandardItem(flag.name)
                child2 = QStandardItem(str(flag.value))
                
                parent1.appendRow([child1, child2])
            
            self.data_model.appendRow(parent1)
            
        
class Custom_scroll_Layout(QVBoxLayout):
    """Create a ScrollAreaLyout containing a layout passed by parameter
    """
    
    def __init__(self, layout) :
        """Constructor
        
        Parameter
        ---------
        layout : QTLayout
            the layout to be in the scrolled area 
        """
        

        super().__init__()
        
        #The widget that contains the grid layout
        self.__layout = layout
        self.__scrollWidget = QWidget()
        self.__scrollWidget.setLayout(self.__layout)

        
        # the scrollarea the contains the previous widget
        self.__scroll = QScrollArea()
        self.__scroll.setWidget(self.__scrollWidget)
        self.__scroll.setWidgetResizable(True)
        self.__scroll.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        
        # the layout that contains the scrollarea
        self.addWidget(self.__scroll)


class ColorASMXEditorDialog(QDialog):
    """A dialog box used to set manually the editor theme
    
    """
    def __init__(self, parrent = None):
        """constructor
        
        Parameter
        ---------
        parrent : Qwidget
            the parrent widget of the dialog box 
        """
        super().__init__()
        
        self.v_layout = QVBoxLayout()
        
        mapper = QSignalMapper(self);
        
        self.theme = parrent.theme
        
        self.parrent = parrent
        
        self.list_color = []
        self.list_key = []
        
        i = 0
        for key in self.theme.attributes.keys() :
            hbox = QHBoxLayout()
            
            button = QPushButton()
            button.setText(key)
            
            value = getattr(self.theme, key)
            
            frame = QFrame()
            frame.setStyleSheet("QWidget { background-color: %s }" 
            % value.name())
            
            self.list_color.append(value)
            self.list_key.append(key)
            
            button.clicked.connect(mapper.map)
            mapper.setMapping(button, i)
            
            # connect(button, SIGNAL("clicked()"), mapper, SLOT("map()"))
            # mapper.setMapping(button, i)
            
            hbox.addWidget(button)
            hbox.addWidget(frame)
            
            self.v_layout.addLayout(hbox)
            i += 1
        
        mapper.mapped[int].connect(self.showDialog)
        
        hbox = QHBoxLayout()
        
        accept = QPushButton()
        accept.setText("Appliquer")
        reject = QPushButton("Annuler")
        
        accept.clicked.connect(self.accept_change)
        reject.clicked.connect(self.abord_change)
        
        hbox.addWidget(accept)
        hbox.addWidget(reject)
        
        self.v_layout.addLayout(hbox)
        
        self.setLayout(self.v_layout)
        
        self.setGeometry(300, 300, 250, 180)
        self.setWindowTitle('Color dialog')
        # self.show()
        
    def accept_change(self):
        """Function to update change of current toolbox
        """
        
        self.parrent.set_theme(self.theme)
        
        self.done(1)
        
    def abord_change(self):
        """Function to aboard changes of current toolbox
        """
        
        self.done(0)
        
        
    def showDialog(self, index):
        """Function to proceed color change of theme
        
        Parameters :
        ------------
        index : integer
            the index of the object to proceed changes
        
        """
      
        col = QColorDialog.getColor(self.list_color[index])

        if col.isValid():
            
            setattr(self.theme, self.list_key[index], col)
        
            self.v_layout.itemAt(index).itemAt(1).widget().setStyleSheet("QWidget { background-color: %s }"
                % col.name())
            print("[utilis-ColorASMXEditorDialog-showDialog]")
            for key in self.theme.attributes.keys() :
                
                print("{} ::: {}".format(key, getattr(self.theme, key).name()))
            print("")
                
