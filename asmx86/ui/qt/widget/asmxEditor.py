
# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHAN__tabILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#   Modules - System
#-------------------------------------------------------------------------------

import sys

#-------------------------------------------------------------------------------
#   Modules - PyQT5
#-------------------------------------------------------------------------------

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

#-------------------------------------------------------------------------------
#   Modules - ASMx86
#-------------------------------------------------------------------------------


#syntaxic coloration
from asmx86.ui.qt.widget.asmxSyntax import AsmxSyntax

#num lines and highlight current line
from asmx86.ui.qt.widget.editor import CodeEditor

#-------------------------------------------------------------------------------
#   Class
#-------------------------------------------------------------------------------
class AsmxEditor(CodeEditor):

    def __init__(self, theme):

        super().__init__()
        #self.__syntax_widget = PythonHighlighter(self.__zone_edit.document())

        
        self.font_size = 10
        self.font_name = "Liberation mono"

        self.__font = QFont(self.font_name)

        self.setFont(self.__font)
        self.setFontSize(self.font_size)
        self.setTabSize(4)
        
        self.set_theme(theme)
    
    def setCursorPosition(self, position):
        
        curs = QTextCursor(self.document().findBlockByLineNumber(position-1))
            
        self.setTextCursor(curs)
        
        
    def set_theme(self, theme) :
        
        self.background = theme.background
        self.refresh_color()
        self.__syntax_widget = AsmxSyntax(self.document(), theme)
        self.init_colors(theme)
        
    def refresh_color(self):
        
        palette = self.palette()
        palette.setColor(QPalette.Base, self.background)
        self.setPalette(palette)
        
    def print_font_info(self):
        
        print(self.__font.toString())
        
        print("---------------------------")
        print("Default family : {0}".format(self.__font.defaultFamily()))
        print("Family : {0}".format(self.__font.family()))
        print("key: {0}".format(self.__font.key()))
        print("lastResortFamily: {0}".format(self.__font.lastResortFamily()))
        
    
    def set_font(self,font) :

        self.__font = font
        self.setFont(self.__font)
        self.font_name = self.__font.family()
        self.font_size = int(self.__font.key().split(',')[1])
    
    def get_font(self) :
        
        return self.__font

    def setTabSize(self, size):

        fontmetrics = QFontMetrics(self.__font)
        self.setTabStopWidth(size * fontmetrics.width(' '))

    def setFontSize(self, size):
        self.font_size = size
        self.__font.setPointSize(self.font_size)
        self.setFont(self.__font)

    def fontSize(self):
        return self.font_size
