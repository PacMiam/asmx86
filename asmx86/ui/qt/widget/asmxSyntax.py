
# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHAN__tabILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#   Modules - System
#-------------------------------------------------------------------------------

import sys

#-------------------------------------------------------------------------------
#   Modules - PyQT5
#-------------------------------------------------------------------------------

from PyQt5.QtGui import *
from PyQt5.QtCore import *

#-------------------------------------------------------------------------------
#   Class
#-------------------------------------------------------------------------------

def format(color, style=''):
    """Return a QTextCharFormat with the given attributes.
    """
    # _color = QColor()
    # _color.setNamedColor(color)
    _format = QTextCharFormat()

    _format.setForeground(color)
    if 'bold' in style:
        _format.setFontWeight(QFont.Bold)
    if 'italic' in style:
        _format.setFontItalic(True)

    return _format


class AsmxSyntax(QSyntaxHighlighter):
    """syntactic coloration widget for asm/nasm language 
    """

    INSTRUCTIONS = [
        'db','push','mov','ret','call','pop','int'
    ]

    KEYWORDS = [
        'extern','section','global'
    ]

    REGISTERS = [
        'edx','ebx','ecx','eax',
    ]

    def __init__(self, document_txt, theme):
        """ constructor
        
        parameters
        ----------
        
        document_txt : QTextDocument
            the document of the texet zone to applicate syntax highlighting
            
        theme : ThemeColors
            the coloration of the syntax as a ThemeColors object
        """
        
        self.styles = dict()
        self.styles['foreground'] = format(theme.foreground)
        self.styles['instruction'] = format(theme.instruction)
        self.styles['keyword'] = format(theme.keyword)
        self.styles['register'] = format(theme.register)
        self.styles['label'] = format(theme.label, 'bold')
        self.styles['comment'] = format(theme.comment, 'italic')
        self.styles['numbers'] = format(theme.numbers)

        QSyntaxHighlighter.__init__(self,document_txt)

        rules = []
        
        rules += [(r'[^\n]+', 0, self.styles['foreground'])]

        rules += [(r'\b%s\b' % instr, 0, self.styles['instruction'])
            for instr in self.INSTRUCTIONS]

        rules += [(r'\b%s\b' % key, 0, self.styles['keyword'])
            for key in self.KEYWORDS]

        rules += [(r'\b%s\b' % reg, 0, self.styles['register'])
            for reg in self.REGISTERS]

        rules += [
            (r'\b[+-]?[0-9]+[lL]?\b', 0, self.styles['numbers']),
            (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0, self.styles['numbers']),
            (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0, self.styles['numbers']),
            (r'([a-zA-Z0-9]|[_])*([a-zA-Z0-9])+:', 0, self.styles['label']),
            (r';.*', 0, self.styles['comment']),
        ]
        
        #tranform pathers in QRegExp
        self.rules = [(QRegExp(pathern), index, form)
            for (pathern, index, form) in rules]
    
    def highlightBlock(self, text):
            """Apply syntax highlighting to the given block of text.
            
            parameters
            ----------
            
            text : 
            
                TODO:---
            """
            # Do other syntax formatting
            for expression, pos, format in self.rules:
                index = expression.indexIn(text, 0)

                while index >= 0:
                    # We actually want the index of the pos match
                    index = expression.pos(pos)
                    length = len(expression.cap(pos))
                    self.setFormat(index, length, format)
                    index = expression.indexIn(text, index + length)

            self.setCurrentBlockState(0)
