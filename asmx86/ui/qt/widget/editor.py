#Source
#https://doc.qt.io/archives/qt-4.8/qt-widgets-codeeditor-example.html
#-------------------------------------------------------------------------------
#   Modules - System
#-------------------------------------------------------------------------------

import sys

#-------------------------------------------------------------------------------
#   Modules - PyQT5
#-------------------------------------------------------------------------------

from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

#-------------------------------------------------------------------------------
#   Class
#-------------------------------------------------------------------------------

class LineNumberArea(QWidget):
    """Widget to show lines numbers in the side of a texte area
    """
    def __init__(self, editor):
        super().__init__(editor)
        self.editor = editor


    def sizeHint(self):
        """Calculate the size of the LineNumberArea widget

        Returns
        -------
        Qsize
            the size of the LineNumberArea widget in Qsize
        """
        return Qsize(self.editor.lineNumberAreaWidth(), 0)


    def paintEvent(self, event):
        """Paint the LineNumberArea when event paintEvent is emit

        Parameters
        ----------
        event : event
            the paintEvent
        """
        self.editor.lineNumberAreaPaintEvent(event)


class CodeEditor(QPlainTextEdit):
    """Main class of CodeEditor Widget, show liens numbers and highlight current
        line
    """
    def __init__(self):
        super().__init__()
        self.lineNumberArea = LineNumberArea(self)

        self.blockCountChanged.connect(self.updateLineNumberAreaWidth)
        self.updateRequest.connect(self.updateLineNumberArea)
        self.cursorPositionChanged.connect(self.highlightCurrentLine)

        self.updateLineNumberAreaWidth()
        self.init_colors()
        
    
    def init_colors(self, theme = None):
        """initialize colors of editor widgets
            
        Parameters
        ----------
        (obtionnal)
        theme : ThemeColors
                the colors of widgets as ThemeColors object
        """

        if theme is not None :
            self.over = theme.over
            self.lineNumberBackground = theme.lineNumberBackground
            self.lineNumberForeground = theme.lineNumberForeground
        else :
            self.over = QColor(Qt.blue).lighter(180)
            self.lineNumberBackground = Qt.lightGray
            self.lineNumberForeground = Qt.black

    def lineNumberAreaWidth(self):
        """Calculate the width of line numers area according to the size in char
            of the longest line number

        Returns
        -------
        space : int
            the size of the number of char the largest line number in intger
        """
        digits = 1
        count = max(1, self.blockCount())
        while count >= 10:
            count /= 10
            digits += 1
        space = 3 + self.fontMetrics().width('9') * digits
        return space


    def updateLineNumberAreaWidth(self):
        """Update the line number area width
        """
        self.setViewportMargins(self.lineNumberAreaWidth(), 0, 0, 0)


    def updateLineNumberArea(self, rect, dy):
        """Update the line numer area

        Prameters
        ---------
        rect : QRect
            line number area surface to draw in QRect
        dy: int
            the position of rect to update in integer

        """

        if dy:
            self.lineNumberArea.scroll(0, dy)
        else:
            self.lineNumberArea.update(0, rect.y(), self.lineNumberArea.width(),
                                       rect.height())

        if rect.contains(self.viewport().rect()):
            self.updateLineNumberAreaWidth()


    def resizeEvent(self, event):
        """
        Parameters
        ----------
        event : event
            the resize window event
        """
        super().resizeEvent(event)

        cr = self.contentsRect();
        self.lineNumberArea.setGeometry(QRect(cr.left(), cr.top(),
                                        self.lineNumberAreaWidth(), cr.height()))

    def lineNumberAreaPaintEvent(self, event):
        """
        Parameters
        ----------
        event : event
            the paint event
        """

        painter = QPainter(self.lineNumberArea)
        # color
        painter.fillRect(event.rect(), self.lineNumberBackground)

        block = self.firstVisibleBlock()
        blockNumber = block.blockNumber()
        top = self.blockBoundingGeometry(block).translated(self.contentOffset()).top()
        bottom = top + self.blockBoundingRect(block).height()


        height = self.fontMetrics().height()

        while block.isValid() and (top <= event.rect().bottom()):
            if block.isVisible() and (bottom >= event.rect().top()):
                number = str(blockNumber + 1)
                painter.setPen(self.lineNumberForeground)
                painter.drawText(0, top, self.lineNumberArea.width(), height,
                                 Qt.AlignRight, number)

            block = block.next()
            top = bottom
            bottom = top + self.blockBoundingRect(block).height()
            blockNumber += 1


    def highlightCurrentLine(self):
        """Higlight the current line of the CodeEditor
        """
        extraSelections = []

        if not self.isReadOnly():
        
            selection = QTextEdit.ExtraSelection()
            #color

            selection.format.setBackground(self.over)
            selection.format.setProperty(QTextFormat.FullWidthSelection, True)
            selection.cursor = self.textCursor()
            selection.cursor.clearSelection()
            extraSelections.append(selection)

        self.setExtraSelections(extraSelections)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    txt = CodeEditor()
    txt.show()

    sys.exit(app.exec_())
