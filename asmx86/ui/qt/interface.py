# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHAN__tabILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
#   Modules - System
#-------------------------------------------------------------------------------

import sys

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser

# Time
from time import sleep

#-------------------------------------------------------------------------------
#   Modules - PyQT5
#-------------------------------------------------------------------------------

from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

from asmx86.ui.qt.utils import *

#-------------------------------------------------------------------------------
#   Modules - ASMx86
#-------------------------------------------------------------------------------

from asmx86.ui.utils import *

#syntaxic coloration
from asmx86.ui.qt.widget.asmxSyntax import AsmxSyntax
from asmx86.ui.qt.widget.asmxEditor import AsmxEditor

#num lines and highlight current line
from asmx86.ui.qt.widget.editor import CodeEditor
from asmx86.lib.engine import Engine
from asmx86.lib.flag import *

#-------------------------------------------------------------------------------
#   Class
#-------------------------------------------------------------------------------

class Interface(object):
    """The main interface
    """

    def __init__(self, file_path, debug):

        app = QApplication(sys.argv)
        ex = window(file_path, debug)
        app.exec_()

class ThemeColors(object):
    """Implementation of the theme for the asmxEditor

    Attributes
    -----------
    string - QColor :
        key, name of a part of editor to be colored by the attribute color
    """

    attributes = {
        'instruction': QColor('#ff00cc'),
        'keyword': QColor('green'),
        'register': QColor('blue'),
        'label': QColor('magenta'),
        'comment': QColor('darkGreen'),
        'numbers': QColor('brown'),
        'foreground': QColor('black'),
        'background' : QColor('white'),
        'lineNumberForeground' : QColor('black'),
        'lineNumberBackground' : QColor('lightGrey'),
        'over' : QColor('lightGrey'),
    }

    def __init__(self, config = None):
        """constructor

        Parameter:
        ----------
        config : TODO
            TODO
        """

        for key, value in self.attributes.items() :
            setattr(self, key, value)

            if config is not None and config.has_option("color", key):
                data = config.get("color", key, fallback = value)
                setattr(self, key, Qcolor(data))
        # TODO la suite

class window(QMainWindow):
    """Main application window
    """

    def __init__(self, file_path, debug, config = None):
        """ Constructor
        """
        super().__init__()

        self.__file_path = file_path
        self.__debug = debug
        self.__config = config

        self.initUI()


    def initUI(self):
        """ Initialize of the UI
        """


        #https://doc.qt.io/qt-5/stylesheet-reference.html

        #-----------------------------------------------------------------
        # Exec Toolbar
        #-----------------------------------------------------------------
        self.prev_action = ToolBarAction('media-seek-backward', 'ligne précédente',
             self.prev_line, self)
        self.next_action = ToolBarAction('media-seek-forward', 'ligne suivante',
             self.next_line, self)
        self.exec_action = ToolBarAction('media-playback-start', 'Executer ',
             self.exec_prog, self)

        self.execToolBar = QToolBar()
        self.execToolBar.addAction(self.exec_action)
        self.execToolBar.addAction(self.prev_action)
        self.execToolBar.addAction(self.next_action)

        #-----------------------------------------------------------------
        # Init Tab Execution
        #-----------------------------------------------------------------
        self.__exec_tab = QTabWidget()
        self.__exec_zone = QWidget()

        self.__exec_zone_layout = QVBoxLayout()
        self.__exec_zone.setLayout(self.__exec_zone_layout)

        self.__exec_zone_layout.addWidget(self.execToolBar)
        self.__exec_zone_layout.addWidget(self.__exec_tab)


        #-----------------------------------
        # Exec Tab
        #-----------------------------------
        self.__tab_exec = QWidget()
        self.__exec_tab.addTab(self.__tab_exec, "Execution")

        self.__exec_edit = QPlainTextEdit()
        self.__exec_edit.setReadOnly(True)

        v_exec_layout = QVBoxLayout()

        v_exec_layout.addWidget(self.execToolBar)
        v_exec_layout.addWidget(self.__exec_edit)

        #the tab wiget the contains the previous layout
        self.__tab_exec.setLayout(v_exec_layout)


        #-----------------------------------
        # Parser Tab
        #-----------------------------------

        self.__tab_output = QWidget()
        self.__exec_tab.addTab(self.__tab_output, "Parser")

        self.__output_view = QTreeView()
        self.__output_view.setHeaderHidden(True)
        self.__output_view.setEditTriggers(QAbstractItemView.NoEditTriggers)

        h_output_layout = QHBoxLayout()
        h_output_layout.addWidget(self.__output_view)

        self.__tab_output.setLayout(h_output_layout)


        #-----------------------------------
        # Status Tab
        #-----------------------------------
        self.__tab_status = QWidget()
        self.__exec_tab.addTab(self.__tab_status, "Status")

        self.__status_view = QTreeView()
        self.__status_view.setHeaderHidden(True)
        self.__status_view.setEditTriggers(QAbstractItemView.NoEditTriggers)

        h_status_layout = QHBoxLayout()
        h_status_layout.addWidget(self.__status_view)

        self.__status_message_model = QStandardItemModel()
        self.__status_message_model.setHorizontalHeaderLabels(['Message'])

        self.__status_view.setModel(self.__status_message_model)

        #the tab wiget the contains the previous layout
        self.__tab_status.setLayout(h_status_layout)


        #-----------------------------------------------------------------
        #Zone Tab Left Side
        #-----------------------------------------------------------------

        self.__tab_zone_side = QTabWidget()

        self.__tab_symbols_side = QWidget()
        self.__tab_zone_side.addTab(self.__tab_symbols_side, "Symbols")

        self.__symbol_tree = QTreeView()
        self.__symbol_tree.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.__symbol_tree.setUniformRowHeights(True)
        self.__symbol_tree.expandAll()
        self.__symbol_tree.setEditTriggers(QAbstractItemView.NoEditTriggers)

        layout = QHBoxLayout()
        layout.addWidget(self.__symbol_tree)

        self.__tab_symbols_side.setLayout(layout)



        self.__tab_status_side = QWidget()
        self.__tab_zone_side.addTab(self.__tab_status_side, "Data")

        self.__reg_tree = QTreeView()
        self.__reg_tree.setSelectionBehavior(QAbstractItemView.SelectRows)

        self.__reg_tree.setUniformRowHeights(True)
        self.__reg_tree.expandAll()
        self.__reg_tree.setEditTriggers(QAbstractItemView.NoEditTriggers)

        layout = QHBoxLayout()
        layout.addWidget(self.__reg_tree)

        self.__tab_status_side.setLayout(layout)

        #-----------------------------------------------------------------
        # Code Editor Zone
        #-----------------------------------------------------------------

        self.__tab__files = QTabWidget()
        self.__tab__files.setTabsClosable(True)

        self.theme = ThemeColors(self.__config)

        for path in self.__file_path:
            self.open_file_tab(path, self.__debug)

        #-----------------------------------------------------------------
        # Connect signals
        #-----------------------------------------------------------------
        self.__tab__files.tabCloseRequested.connect(self.askCloseTab)
        self.__tab__files.currentChanged.connect(self.currentTabChanged)
        self.__reg_tree.doubleClicked.connect(self.reg_edit)

        #-----------------------------------------------------------------
        #layout Definition
        #-----------------------------------------------------------------

        __hbox = QHBoxLayout()

        #widget AsmxEditor
        __splitter1 = QSplitter(Qt.Horizontal)
        __splitter1.addWidget(self.__tab_zone_side)
        __splitter1.addWidget(self.__tab__files)
        __splitter1.setCollapsible(1,False)

        #setStretchFactor used to set ration of different widgets
        __splitter1.setStretchFactor(0,1)
        __splitter1.setStretchFactor(1,5)

        __splitter2 = QSplitter(Qt.Vertical)
        __splitter2.addWidget(__splitter1)
        __splitter2.addWidget(self.__exec_zone)

        __splitter2.setStretchFactor(0,2)
        __splitter2.setStretchFactor(1,1)
        __splitter2.setCollapsible(0,False)

        __hbox.addWidget(__splitter2)


        #correct way to define main widget/layout
        __window = QWidget()
        __window.setLayout(__hbox)


        self.setCentralWidget(__window)

        self.setGeometry(500, 200, 1024, 768)
        self.setWindowTitle('TestWindow')

        #-----------------------------------------------------------------
        # Menus
        #-----------------------------------------------------------------

        self.__open_file = MenuAction("&Open", "Ctrl+O",
             'open a file', self.file_open, self)
        self.__save_file = MenuAction("&Save", "Ctrl+S",
             'save current file', self.file_save, self)
        self.__save_file_as = MenuAction("&Save as ...", "Ctrl+Shift+S",
             'save current file as', self.file_save_as, self)
        self.__exit_act = MenuAction("&Exit", "Ctrl+Q",
             'Exit application', self.close_application, self)
        self.__font_act = MenuAction("&Font", "",
             'Change code font', self.font_dialog, self)
        self.__color_act = MenuAction("&Color", "",
             'Change code font color theme', self.color_set_dialog, self)

        __menu_bar = self.menuBar()
        __file_menu = __menu_bar.addMenu('&File')
        __file_menu.addAction(self.__open_file)
        __file_menu.addAction(self.__save_file)
        __file_menu.addAction(self.__save_file_as)
        __file_menu.addAction(self.__exit_act)
        __file_menu = __menu_bar.addMenu('&Format')
        __file_menu.addAction(self.__font_act)
        __file_menu.addAction(self.__color_act)



        #-----------------------------------------------------------------
        # Default Toolbar
        #-----------------------------------------------------------------
        """
            icon theme list :
            https://specifications.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
        """

        new_action = ToolBarAction('document-new', 'New',
             self.new_file, self)
        open_action = ToolBarAction('document-open', 'Open',
             self.file_open, self)
        save_action = ToolBarAction('document-save', 'Save',
             self.file_save, self)
        exit_action = ToolBarAction('application-exit', 'Quitter',
             self.close_application, self)
        copy_action = ToolBarAction('edit-copy', 'Copy',
             self.copy, self)
        cut_action = ToolBarAction('edit-cut', 'Cut',
             self.cut, self)
        paste_action = ToolBarAction('edit-paste', 'Paste',
             self.paste, self)
        undo_action = ToolBarAction('edit-undo', 'undo',
             self.undo, self)
        redo_action = ToolBarAction('edit-redo', 'redo',
             self.redo, self)

        self.toolBar = self.addToolBar("New")
        self.toolBar.addAction(new_action)
        self.toolBar = self.addToolBar("File")
        self.toolBar.addAction(open_action)
        self.toolBar.addAction(save_action)
        self.toolBar = self.addToolBar("Edit")
        self.toolBar.addAction(copy_action)
        self.toolBar.addAction(cut_action)
        self.toolBar.addAction(paste_action)
        self.toolBar.addAction(undo_action)
        self.toolBar.addAction(redo_action)

        self.left_spacer = QWidget()
        self.left_spacer.setSizePolicy(QSizePolicy.Expanding,
                QSizePolicy.Expanding)

        self.toolBar = self.addToolBar("Paste")
        self.toolBar.addWidget(self.left_spacer)
        self.toolBar.addAction(exit_action)



        #-----------------------------------------------------------------
        # Status Bar init
        #-----------------------------------------------------------------

        self.__status_bar = self.statusBar()

        self.status_wiget = QWidget()
        status_h_box = QHBoxLayout()

        self.__line_label = QLabel("")
        self.__column_label = QLabel("")
        self.__font_label = QLabel("")

        self.__line_label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.__column_label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.__font_label.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)


        status_h_box.addWidget(HBoxSeparatorGenerator().generate_separator())
        status_h_box.addWidget(self.__line_label)
        status_h_box.addWidget(HBoxSeparatorGenerator().generate_separator())
        status_h_box.addWidget(self.__column_label)
        status_h_box.addWidget(HBoxSeparatorGenerator().generate_separator())
        status_h_box.addWidget(self.__font_label)

        self.status_wiget.setLayout(status_h_box)
        self.status_wiget.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.__status_bar.addPermanentWidget(self.status_wiget)

        self.update_status()

        #-----------------------------------------------------------------
        # Engine init
        #-----------------------------------------------------------------
        self.setWindowTitle("ASMx86")
        self.show()
        self.refresh_data()

        if self.__debug :
            print("[Engine Init] font info : ")
            self.__tab__files.currentWidget().print_font_info()


    def open_file_tab(self, file_path, debug) :
        """Open a file ine the app

        Parameters:
        -----------
        file_path : string
            the path of the file to be opened as a string
        debug : bool
            True if app lunched in debug mod, else False
        """

        zone_edit = AsmxTabEditor(self.theme, file_path, debug)
        # self.__tab__files.currentWidget().set_theme(theme)
        zone_edit.cursorPositionChanged.connect(self.update_status)
        zone_edit.textChanged.connect(self.update_temp_side)
        i = int()

        if zone_edit.file_path is not None :

            i = self.__tab__files.addTab(zone_edit, "{}".format(basename(zone_edit.file_path)))
            self.add_status_message("Open : {} file".format(zone_edit.file_path))
        else :
            i = self.__tab__files.addTab(zone_edit, "Untitled")
            self.add_status_message("New : Untitled file")


        self.__tab__files.setCurrentIndex(i)
        message = self.__tab__files.currentWidget().engine.parse()
        self.set_parsing_message(message)
        self.update_barr_exec()


    def set_theme(self,theme) :
        """change the theme of the asmxEditor

        Parameters :
        ------------
        theme : ThemeColors
            the theme to apply to the asmxEditor
        """

        self.theme = theme

        self.__tab__files.currentWidget().set_theme(theme)



    def refresh_data(self) :
        """Refresh the data in the tab Widgets

        Parameters:
        -----------
        models : list of models
            List of models to be refreshed
        """

        # tab side TODO = update on code change
        if self.__tab__files.count() > 0 :

            currentWidget = self.__tab__files.currentWidget()

            if currentWidget.engine is not None:

                currentWidget.refresh_data()

                self.__symbol_tree.setModel(currentWidget.symbol_model)
                self.__symbol_tree.selectionModel().selectionChanged.connect(self.var_tree_selection)
                self.__symbol_tree.expandAll()

                #-----------------------------------------------------------------
                # Tab Zone Data
                #-----------------------------------------------------------------
                # if currentWidget.data_model is not None :
                    # if not currentWidget.edit_reg_signal_connected :
                currentWidget.data_model.dataChanged.connect(self.on_finished_edit_reg)
                currentWidget.edit_reg_signal_connected = True

                self.__reg_tree.setModel(currentWidget.data_model)
                self.__reg_tree.expandAll()

                #print("[qt-interface-refresh-data] into refresh exec tab ")
                self.refresh_exec_tab()

        else :

            symbol_model = QStandardItemModel()
            symbol_model.setHorizontalHeaderLabels(['Name', 'Value', 'Row'])
            data_model = QStandardItemModel()
            data_model.setHorizontalHeaderLabels(['Name', 'Value'])

            self.__symbol_tree.setModel(symbol_model)
            self.__reg_tree.setModel(data_model)


    def refresh_exec_tab(self) :
        """Refresh the content of the exec tab
        """


        if self.__tab__files.count() > 0 and self.__tab__files.currentWidget().engine is not None:


            #-----------------------------------
            # Exec Tab
            #-----------------------------------

            # self.__exec_view.setModel(self.__tab__files.currentWidget().execute_model)

            self.__exec_edit.setPlainText(self.__tab__files.currentWidget().execute_buffer)

            #-----------------------------------
            # Parser Tab
            #-----------------------------------

            # self.__output_edit.setPlainText(self.__tab__files.currentWidget().output_buffer)
            self.__output_view.setModel(self.__tab__files.currentWidget().output_model)


    def var_tree_selection(self, newItemSelec, OldItemSelec):
        """Change cursor position according to the line number of item clicked
           in the sidebarr

        Parameters :
        ------------
        newItemSelec : QModelIndex
            the model index of the current selected line in treeview
        OldItemSelec : QModelIndex
            the model index of the previous selected line in treeview
        """

        data = self.__symbol_tree.model().data(newItemSelec.indexes()[2])

        if data is not None :
            self.__tab__files.currentWidget().setCursorPosition(int(data))


    def reg_edit(self, model_index) :
        """Enable the edit of the registry treeView value

        Parameters :
        ------------
        model_index : QModelIndex
            the index of the value to edit
        """

        data = self.__reg_tree.selectionModel().selectedIndexes()
        if len(data) == 2 :
            self.__reg_tree.edit(data[1])


    def on_finished_edit_reg(self, model_index_top_teft, model_index_bottom_right, vector_roles) :
        """Save the edited value into engine
        Parameters :
        ------------
        model_index_top_teft: QModelIndex
            the first index of edited values

        model_index_bottom_right : QModelIndex
            the last index of edited values, same as model_index_top_teft
            if only one value edited (in this function always one value is edited)

        vector_roles : Vector of Int
            the role of all values in interval defined in Qt
        """

        name = self.__reg_tree.selectionModel().selectedIndexes()[0].data()
        value = model_index_top_teft.data()

        currentWidget = self.__tab__files.currentWidget()

        if currentWidget.engine.has_register(name) :

            if self.RepresentsInt(value) :
                currentWidget.engine.set_register_value(name, int(value))
            else :
                currentWidget.engine.set_register_value(name, value)

        else :

            print("error : register \"{}\" not exists".format(name))


    def file_open(self):
        """Open a file to be used in the application
        """
        #Return of QFileDialog is a tuple
        name = QFileDialog.getOpenFileName(self, 'Ouvrir un Fichier', '',
                             "ASM Files (*.nasm *.asm) ")

        if name[0] :

            self.open_file_tab(name[0],self.__debug)
            self.refresh_data()

    def file_save(self):
        """Save the document of the current tab
        """

        if self.__tab__files.count() > 0:

            if self.__tab__files.currentWidget().engine is not None and self.__tab__files.currentWidget().file_path is not None:

                    currentWidget = self.__tab__files.currentWidget()

                    self.__current_buffer = currentWidget.toPlainText()

                    self.__engine_buffer = '\n'.join(currentWidget.engine.get_buffer())

                    currentWidget.engine.save(self.__current_buffer)
                    messages = currentWidget.engine.parse(self.__current_buffer.split('\n'))
                    self.set_parsing_message(messages)
                    currentWidget.edited = False

                    self.add_status_message("Save : {} file".format(self.__tab__files.currentWidget().file_path))

                    self.refresh_data()


            else:

                self.file_save_as()


    def file_save_as(self):
        """Save current opened file
        """
        previous_file_name = ""

        file_path = self.__tab__files.currentWidget().file_path

        if file_path is not None :
            previous_file_name = basename(file_path)
        else :
            previous_file_name = "Untitled"

        if self.__tab__files.count() > 0:

            name = QFileDialog.getSaveFileName(self, 'Enregistrer sous ...', '',
                                 "ASM Files (*.nasm *.asm) ")

            if name[0] :
                currentWidget = self.__tab__files.currentWidget()
                currentWidget.file_path = name[0]
                self.__current_buffer = currentWidget.toPlainText()
                currentWidget.engine.set_path(currentWidget.file_path)
                currentWidget.engine.save(self.__current_buffer)
                messages = currentWidget.engine.parse(self.__current_buffer.split('\n'))
                self.set_parsing_message(messages)
                currentWidget.edited = False

                self.refresh_data()

                file_path = self.__tab__files.currentWidget().file_path

                if file_path is not None :
                    file_name = basename(file_path)
                else :
                    print("Error in file_save_as : destination path is None")

                self.__tab__files.setTabText(self.__tab__files.currentIndex(),
                    "{}".format(file_name))

                self.add_status_message("Save : {} file in {}".format(previous_file_name,
                    file_path))


    def new_file(self):
        """ Create a new file
        """
        self.open_file_tab(None, self.__debug)


    def close_application(self):
        """Close the Application
        """

        for i in range(0, self.__tab__files.count()) :

            if self.__tab__files.widget(i).unsaved_edit() :

                self.__tab__files.setCurrentIndex(i)

                path = self.__tab__files.currentWidget().file_path

                if path is None :
                    path = "Untitle"
                else :
                    path = basename(path)

                choice = CustomQtQuestionBox('Sauvegarde',
                    "Des modofications on été apporté à {} \nVoulez vous les sauvegarder ?".format(
                        path),
                        'Oui', 'Non', self.file_save, None)

                choice.run()

        choice = CustomQtQuestionBox('Fermeture', "Voulez vous quitter l'application ?"
                , 'Oui', 'Non', sys.exit, None)
        choice.run()


    def update_status(self):
        """Update informations in the Status Barr
        """

        if self.__tab__files.currentWidget() is not None:
            col = self.__tab__files.currentWidget().textCursor().columnNumber()
            line = self.__tab__files.currentWidget().textCursor().blockNumber() + 1
            maxLine = self.__tab__files.currentWidget().blockCount()

            self.__line_label.setText("line : {} / lines {}".format(line, maxLine))
            self.__column_label.setText("column : {}".format(col))
            self.__font_label.setText("police : {}    taille : {}".format(self.__tab__files.currentWidget().font_name, self.__tab__files.currentWidget().font_size))


    def update_temp_side(self) :
        """update data in sidebar when editor buffer edited
        """
        currentWidget = self.__tab__files.currentWidget()
        messages = currentWidget.engine.parse(currentWidget.toPlainText().split('\n'))
        self.set_parsing_message(messages)
        self.refresh_data()
        currentWidget.edited = True


    def update_barr_exec(self):

        currentWidget = self.__tab__files.currentWidget()

        if currentWidget is not None :

            self.exec_action.setEnabled(True)

            self.prev_action.setEnabled(currentWidget.engine.can_previous_step())
            self.next_action.setEnabled(currentWidget.engine.can_next_step())

        else :

            self.exec_action.setEnabled(False)
            self.prev_action.setEnabled(False)
            self.next_action.setEnabled(False)


    def font_dialog(self) :
        """doalog bot to select font of editor
        """

        font, ok = QFontDialog().getFont(self.__tab__files.currentWidget().get_font())

        if ok:
            self.__tab__files.currentWidget().set_font(font)


    def askCloseTab(self, index) :
        """Function to close a tab, ask to save if code in tab edited

        Parameters :
        ------------
        index : integer
            index of the tab to close
        """
        path = self.__tab__files.widget(index).file_path

        if path is None :
            path = "Untitled"

        if self.__tab__files.widget(index).unsaved_edit() :
            choice = CustomQtQuestionBox('Sauvegarde',
                "Des modofications on été apporté à {} \nVoulez vous les sauvegarder ?".format(
                    basename(path)),
                    'Oui', 'Non', self.file_save, None)

            choice.run()

        self.__tab__files.removeTab(index)


        if path is not None:
            self.add_status_message("Close : {} file".format(path))
        else:
            self.add_status_message("Close : Untitled file")
        self.refresh_data()
        self.update_barr_exec()


    def currentTabChanged(self, index):
        """Refresh the side bar on change tab

        Parameters :
        ------------
        index : integer
            index of the new tab
        """

        self.refresh_data()
        self.update_barr_exec()


    def closeEvent(self,event):
        """
        Parameters
        ----------

        event : event
            the close window event
        """
        for i in range(0, self.__tab__files.count()) :

            if self.__tab__files.widget(i).unsaved_edit() :

                self.__tab__files.setCurrentIndex(i)

                file_path = self.__tab__files.currentWidget().file_path

                if file_path is None :
                    file_path = "Untitled"

                choice = CustomQtQuestionBox('Sauvegarde',
                    "Des modofications on été apporté à {} \nVoulez vous les sauvegarder ?".format(
                        basename(file_path)),
                        'Oui', 'Non', self.file_save, None)
                choice.run()

        choice = CustomQtQuestionBox('Fermeture', "Voulez vous quitter l'application ?"
                , 'Oui', 'Non', sys.exit, event.ignore)
        choice.run()


    def color_set_dialog(self):
        """Open a dialog box to change syntax coloration
        """

        example = ColorASMXEditorDialog(self)
        example.exec_()


    def prev_line(self):
        """Go back of one step in the program execution
        """

        currentWidget = self.__tab__files.currentWidget()

        if currentWidget is not None:
            self.__exec_edit.setPlainText(str())
            self.__tab__files.currentWidget().execute_buffer = \
                self.__exec_edit.toPlainText()

            currentWidget.engine.previous_step()

            for position, message in currentWidget.engine.get_execution_output():

                if message is not None:
                    self.add_execution_message(message)

                if position is not None:
                    self.__tab__files.currentWidget().setCursorPosition(position+1)

                self.refresh_data()

            self.update_barr_exec()

    def next_line(self):
        """Go ahead of one step in program execution
        """

        currentWidget = self.__tab__files.currentWidget()

        if currentWidget is not None:
            self.__exec_edit.setPlainText(str())
            self.__tab__files.currentWidget().execute_buffer = \
                self.__exec_edit.toPlainText()

            currentWidget.engine.next_step()

            for position, message in currentWidget.engine.get_execution_output():

                if message is not None:
                    self.add_execution_message(message)

                if position is not None:
                    self.__tab__files.currentWidget().setCursorPosition(position+1)

                self.refresh_data()

            self.update_barr_exec()


    def exec_prog(self):
        """Execute the program until the end
        """

        self.__tab__files.currentWidget().engine.reset_execution()

        self.__exec_edit.setPlainText(str())
        self.__tab__files.currentWidget().execute_buffer = \
            self.__exec_edit.toPlainText()

        self.refresh_data()

        if self.__tab__files.count() > 0 :

            self.__tab__files.currentWidget().setEnabled(False)

            main_loop = True

            while main_loop:
                self.__tab__files.currentWidget().engine.next_step()

                #TODO enlever possibiliter de modifier le document ou changer onglet

                self.__exec_edit.setPlainText(str())
                self.__tab__files.currentWidget().execute_buffer = \
                    self.__exec_edit.toPlainText()

                position = None
                for position, message in \
                    self.__tab__files.currentWidget().engine.get_execution_output():

                    if message is not None :
                        self.add_execution_message(message)

                        # Detect the end of program
                        if message[0] == AsmAction.TerminateProgram:
                            main_loop = False

                if position is not None:
                    self.__tab__files.currentWidget().setCursorPosition(position+1)

                self.__tab__files.currentWidget().repaint()

                sleep(0.2)

                self.refresh_data()

            #TODO remettre possibilité de modif le document
            self.__tab__files.currentWidget().setEnabled(True)

            self.update_barr_exec()


    def set_parsing_message(self, messages):
        """Show parsing messages of the engine in the Parser tab

        Parameters :
        ------------
        messages : list of strings
            the messages to be displayed
        """

        model = QStandardItemModel()
        model.setHorizontalHeaderLabels(['Message'])

        for output in messages:
            position, message = get_parser_messages(*output)

            if position is None:
                position = str()

            model.appendRow([QStandardItem("%s | %s" % (
                str(position), str(message)))])

        self.__tab__files.currentWidget().output_model = model

        self.refresh_exec_tab()


    def add_execution_message(self, message):
        """Show the execution messages in the Execution tab

        Parameters :
        ------------
        message : string
            the message to be displayed
        """

        type_message = message[0]

        text = ""

        if type_message == AsmError.InstructionNotImplemented:
            text += "Instruction not implemented\n"

        elif type_message == AsmAction.SendToOutput:
            text += "%s\n" % str(message[1])

        elif type_message == AsmAction.TerminateProgram:
            text += "-- Terminate with status %d --\n" % message[1].value

        self.refresh_exec_tab()

        self.__exec_edit.moveCursor(QTextCursor.End)
        self.__exec_edit.insertPlainText(text)
        self.__exec_edit.moveCursor(QTextCursor.End)

        self.__tab__files.currentWidget().execute_buffer = self.__exec_edit.toPlainText()


    def add_status_message(self, message):
        """Add applications status message in the status tab

        Parameters :
        ------------
        message : string
            the message to be displayed
        """

        time = QTime.currentTime()

        # print(time.toString(Qt.DefaultLocaleLongDate))

        message = time.toString(Qt.DefaultLocaleLongDate) + "   " + message

        self.__status_message_model.appendRow([QStandardItem(message)])
        self.__status_view.setModel(self.__status_message_model)
        print(message)

        index = QModelIndex(self.__status_message_model.index(self.__status_message_model.rowCount()-1,0))
        self.__status_view.scrollTo(index)

    def RepresentsInt(self, str):
        """Function to determinate if the string represents an integer

        Parameters :
        ------------
        str : string
            the string to test

        Returns:
        --------
        bool
            true if str represents an integer, else false
        """

        try:

            int(str)
            return True

        except ValueError:

            return False


    def copy(self) :
        """Copy function of editor
        """

        self.__tab__files.currentWidget().copy()

    def cut(self) :
        """Cut function of editor
        """

        self.__tab__files.currentWidget().cut()

    def paste(self) :
        """Paset function of editor
        """

        self.__tab__files.currentWidget().paste()

    def undo(self) :
        """Undo function of editor
        """

        self.__tab__files.currentWidget().undo()

    def redo(self) :
        """Redo function of editor
        """

        self.__tab__files.currentWidget().redo()

