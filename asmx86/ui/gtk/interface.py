# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Time
from time import sleep

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.ui.gtk import *

from asmx86.ui.gtk.editor import InterfaceTabEditor

from asmx86.ui.utils import *

from asmx86.lib.flag import *

# ------------------------------------------------------------------------------
#   Modules - Translation
# ------------------------------------------------------------------------------

from gettext import gettext as _
from gettext import textdomain
from gettext import bindtextdomain

bindtextdomain("asmx86", get_data("i18n"))
textdomain("asmx86")

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Interface(Gtk.Window):

    def __init__(self, path, debug=False):
        """ Constructor

        Parameters
        ----------
        path : str or None
            File path
        debug : bool, optional
            Debug flag (Default: False)
        """

        Gtk.Window.__init__(self)

        # ------------------------------------
        #   Variable
        # ------------------------------------

        # Debug flag
        self.debug = debug

        # Files path
        self.__files_path = path

        # Buffer storage to detect file modification
        self.__current_buffer = None
        self.__previous_buffer = None

        # Store tab position
        self.__current_tab = None

        # Sidebar treeiters storage
        self.__sidebar_iter = dict()

        # Opened files cache
        self.__files_storage = dict()

        # Initialization launch
        self.__init_launch = True

        self.models_data = {
            "symbols": {
                _("Variables"): "insert-object",
                _("Labels"): "insert-text",
                _("Modules"): "insert-link",
            },
            "data": {
                _("Registers"): "dialog-password",
                _("Flags"): "mark-location",
            }
        }

        # ------------------------------------
        #   Initialize logging
        # ------------------------------------

        self.__logger = logging.getLogger("asmx86")

        # ------------------------------------
        #   Prepare interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init packing
        self.__init_packing()

        # Init signals
        self.__init_signals()

        # Start interface
        self.__start_interface()


    def __init_widgets(self):
        """ Initialize interface widgets
        """

        # ------------------------------------
        #   Main window
        # ------------------------------------

        self.set_title(_("Main Interface"))

        self.set_default_size(1024, 768)

        self.set_wmclass("asmx86", "ASMx86")

        self.set_gravity(Gdk.Gravity.CENTER)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        self.grid = Gtk.Box()

        self.grid_notebook_data = Gtk.Box()
        self.grid_notebook_symbol = Gtk.Box()
        self.grid_notebook_execution = Gtk.Box()

        # Properties
        self.grid.set_spacing(0)
        self.grid.set_orientation(Gtk.Orientation.VERTICAL)

        self.grid_notebook_execution.set_spacing(0)
        self.grid_notebook_execution.set_orientation(Gtk.Orientation.VERTICAL)

        # ------------------------------------
        #   Clipboard
        # ------------------------------------

        self.clipboard = Gtk.Clipboard.get(Gdk.Atom.intern("CLIPBOARD", False))

        # ------------------------------------
        #   Files filter
        # ------------------------------------

        self.__files_filter = Gtk.FileFilter.new()

        # Properties
        self.__files_filter.add_pattern("*.*asm")

        # ------------------------------------
        #   Headerbar
        # ------------------------------------

        self.headerbar = Gtk.HeaderBar()

        self.headerbar_button_application = Gtk.MenuButton()
        self.headerbar_button_menu = Gtk.MenuButton()

        self.headerbar_image_application = Gtk.Image()
        self.headerbar_image_menu = Gtk.Image()

        # Properties
        self.headerbar.set_title("ASMx86")
        self.headerbar.set_show_close_button(True)

        self.headerbar_button_application.set_image(
            self.headerbar_image_application)
        self.headerbar_button_menu.set_image(
            self.headerbar_image_menu)

        self.headerbar_image_application.set_from_icon_name(
            "utilities-terminal", Gtk.IconSize.SMALL_TOOLBAR)
        self.headerbar_image_menu.set_from_icon_name(
            "open-menu", Gtk.IconSize.SMALL_TOOLBAR)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.toolbar = Gtk.Toolbar()

        self.toolbar_item_new = Gtk.ToolButton()
        self.toolbar_item_open = Gtk.MenuToolButton()
        self.toolbar_item_save = Gtk.ToolButton()
        self.toolbar_item_copy = Gtk.ToolButton()
        self.toolbar_item_cut = Gtk.ToolButton()
        self.toolbar_item_paste = Gtk.ToolButton()
        self.toolbar_item_undo = Gtk.ToolButton()
        self.toolbar_item_redo = Gtk.ToolButton()

        self.toolbar_item_search = Gtk.ToolItem()

        self.toolbar_item_expand = Gtk.SeparatorToolItem()

        # Properties
        self.toolbar_item_new.set_icon_name("document-new")
        self.toolbar_item_open.set_icon_name("document-open")
        self.toolbar_item_save.set_icon_name("document-save")
        self.toolbar_item_copy.set_icon_name("edit-copy")
        self.toolbar_item_cut.set_icon_name("edit-cut")
        self.toolbar_item_paste.set_icon_name("edit-paste")
        self.toolbar_item_undo.set_icon_name("edit-undo")
        self.toolbar_item_redo.set_icon_name("edit-redo")

        self.toolbar_item_expand.set_draw(False)
        self.toolbar_item_expand.set_expand(True)

        # ------------------------------------
        #   Toolbar - Open menu
        # ------------------------------------

        self.toolbar_menu_open = Gtk.Menu()

        # Properties
        self.toolbar_item_open.set_menu(self.toolbar_menu_open)

        # ------------------------------------
        #   Toolbar - Search
        # ------------------------------------

        self.toolbar_search_entry = Gtk.SearchEntry()

        # Properties
        self.toolbar_search_entry.set_placeholder_text(_("Search..."))

        # ------------------------------------
        #   Paned
        # ------------------------------------

        self.paned_sidebar = Gtk.Paned()
        self.paned_output = Gtk.Paned()

        # Properties
        self.paned_sidebar.set_position(250)
        self.paned_sidebar.set_wide_handle(True)
        self.paned_sidebar.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.paned_output.set_wide_handle(True)
        self.paned_output.set_orientation(Gtk.Orientation.VERTICAL)

        # ------------------------------------
        #   Notebook - Editor
        # ------------------------------------

        self.notebook_editor = Gtk.Notebook()

        # Properties
        self.notebook_editor.set_scrollable(True)
        self.notebook_editor.set_tab_pos(Gtk.PositionType.TOP)

        # ------------------------------------
        #   Notebook - Sidebar
        # ------------------------------------

        self.notebook_sidebar = Gtk.Notebook()

        # Properties
        self.notebook_sidebar.set_scrollable(True)
        self.notebook_sidebar.set_tab_pos(Gtk.PositionType.TOP)

        # ------------------------------------
        #   Notebook - Document
        # ------------------------------------

        self.label_notebook_symbol = Gtk.Label()

        self.scroll_notebook_symbol = Gtk.ScrolledWindow()

        self.treeview_notebook_symbol = Gtk.TreeView()

        self.column_notebook_symbol = Gtk.TreeViewColumn()

        self.cell_notebook_symbol_icon = Gtk.CellRendererPixbuf()
        self.cell_notebook_symbol_name = Gtk.CellRendererText()
        self.cell_notebook_symbol_value = Gtk.CellRendererText()

        # Properties
        self.label_notebook_symbol.set_label(_("Symbols"))

        self.treeview_notebook_symbol.set_headers_clickable(False)
        self.treeview_notebook_symbol.set_enable_tree_lines(True)
        self.treeview_notebook_symbol.set_headers_visible(False)
        self.treeview_notebook_symbol.set_show_expanders(True)

        self.column_notebook_symbol.pack_start(
            self.cell_notebook_symbol_icon, False)
        self.column_notebook_symbol.pack_start(
            self.cell_notebook_symbol_name, True)
        self.column_notebook_symbol.pack_start(
            self.cell_notebook_symbol_value, False)

        self.column_notebook_symbol.add_attribute(
            self.cell_notebook_symbol_icon, "icon-name", 0)
        self.column_notebook_symbol.add_attribute(
            self.cell_notebook_symbol_name, "markup", 1)
        self.column_notebook_symbol.add_attribute(
            self.cell_notebook_symbol_value, "text", 2)
        self.column_notebook_symbol.add_attribute(
            self.cell_notebook_symbol_value, "visible", 3)
        self.column_notebook_symbol.add_attribute(
            self.cell_notebook_symbol_icon, "visible", 4)

        self.cell_notebook_symbol_icon.set_alignment(0, .5)
        self.cell_notebook_symbol_icon.set_padding(6, 4)
        self.cell_notebook_symbol_name.set_alignment(0, .5)
        self.cell_notebook_symbol_name.set_padding(2, 4)
        self.cell_notebook_symbol_value.set_alignment(1, .5)
        self.cell_notebook_symbol_value.set_padding(6, 4)

        self.cell_notebook_symbol_name.set_property(
            "ellipsize", Pango.EllipsizeMode.END)
        self.cell_notebook_symbol_value.set_property("editable", True)

        self.treeview_notebook_symbol.append_column(
            self.column_notebook_symbol)

        # ------------------------------------
        #   Notebook - Data
        # ------------------------------------

        self.label_notebook_data = Gtk.Label()

        self.scroll_notebook_data = Gtk.ScrolledWindow()

        self.treeview_notebook_data = Gtk.TreeView()

        self.column_notebook_data = Gtk.TreeViewColumn()

        self.cell_notebook_data_icon = Gtk.CellRendererPixbuf()
        self.cell_notebook_data_name = Gtk.CellRendererText()
        self.cell_notebook_data_value = Gtk.CellRendererText()

        # Properties
        self.label_notebook_data.set_label(_("Data"))

        self.treeview_notebook_data.set_headers_clickable(False)
        self.treeview_notebook_data.set_enable_tree_lines(True)
        self.treeview_notebook_data.set_headers_visible(False)
        self.treeview_notebook_data.set_show_expanders(True)

        self.column_notebook_data.pack_start(
            self.cell_notebook_data_icon, False)
        self.column_notebook_data.pack_start(
            self.cell_notebook_data_name, False)
        self.column_notebook_data.pack_start(
            self.cell_notebook_data_value, True)

        self.column_notebook_data.add_attribute(
            self.cell_notebook_data_icon, "icon-name", 0)
        self.column_notebook_data.add_attribute(
            self.cell_notebook_data_name, "markup", 1)
        self.column_notebook_data.add_attribute(
            self.cell_notebook_data_value, "text", 2)
        self.column_notebook_data.add_attribute(
            self.cell_notebook_data_icon, "visible", 3)

        self.cell_notebook_data_icon.set_alignment(0, .5)
        self.cell_notebook_data_icon.set_padding(6, 4)
        self.cell_notebook_data_name.set_alignment(0, .5)
        self.cell_notebook_data_name.set_padding(2, 4)
        self.cell_notebook_data_value.set_alignment(0, .5)
        self.cell_notebook_data_value.set_padding(6, 4)

        self.cell_notebook_data_name.set_property(
            "ellipsize", Pango.EllipsizeMode.END)
        self.cell_notebook_data_value.set_property(
            "ellipsize", Pango.EllipsizeMode.END)
        self.cell_notebook_data_value.set_property("editable", True)

        self.treeview_notebook_data.append_column(
            self.column_notebook_data)

        # ------------------------------------
        #   Notebook - Output
        # ------------------------------------

        self.notebook_output = Gtk.Notebook()

        # Properties
        self.notebook_output.set_scrollable(True)
        self.notebook_output.set_tab_pos(Gtk.PositionType.LEFT)

        # HACK: Avoid to have a small notebook (Temporary fix)
        self.notebook_output.set_size_request(-1, 200)

        # ------------------------------------
        #   Notebook - Status
        # ------------------------------------

        self.label_notebook_status = Gtk.Label()

        self.scroll_notebook_status = Gtk.ScrolledWindow()

        self.model_notebook_status = Gtk.ListStore(str, str)
        self.treeview_notebook_status = Gtk.TreeView()

        self.filter_notebook_status = self.model_notebook_status.filter_new()

        self.column_notebook_status = Gtk.TreeViewColumn()

        self.cell_notebook_status_time = Gtk.CellRendererText()
        self.cell_notebook_status_description = Gtk.CellRendererText()

        # Properties
        self.label_notebook_status.set_label(_("Status"))
        self.label_notebook_status.set_halign(Gtk.Align.END)

        self.treeview_notebook_status.set_model(
            self.filter_notebook_status)
        self.treeview_notebook_status.set_headers_clickable(False)
        self.treeview_notebook_status.set_enable_tree_lines(True)
        self.treeview_notebook_status.set_headers_visible(False)

        self.column_notebook_status.pack_start(
            self.cell_notebook_status_time, False)
        self.column_notebook_status.pack_start(
            self.cell_notebook_status_description, True)

        self.column_notebook_status.add_attribute(
            self.cell_notebook_status_time, "markup", 0)
        self.column_notebook_status.add_attribute(
            self.cell_notebook_status_description, "markup", 1)

        self.cell_notebook_status_time.set_alignment(1, .5)
        self.cell_notebook_status_time.set_padding(6, 4)
        self.cell_notebook_status_description.set_alignment(0, .5)
        self.cell_notebook_status_description.set_padding(6, 4)

        self.cell_notebook_status_time.set_property(
            "family", "Monospace")
        self.cell_notebook_status_description.set_property(
            "ellipsize", Pango.EllipsizeMode.END)

        self.treeview_notebook_status.append_column(
            self.column_notebook_status)

        # ------------------------------------
        #   Notebook - Parser
        # ------------------------------------

        self.label_notebook_parser = Gtk.Label()

        self.scroll_notebook_parser = Gtk.ScrolledWindow()

        self.treeview_notebook_parser = Gtk.TreeView()

        self.column_notebook_parser_position = Gtk.TreeViewColumn()
        self.column_notebook_parser_description = Gtk.TreeViewColumn()

        self.cell_notebook_parser_position = Gtk.CellRendererText()
        self.cell_notebook_parser_description = Gtk.CellRendererText()

        # Properties
        self.label_notebook_parser.set_label(_("Parser"))
        self.label_notebook_parser.set_halign(Gtk.Align.END)

        self.treeview_notebook_parser.set_headers_clickable(False)
        self.treeview_notebook_parser.set_enable_tree_lines(True)
        self.treeview_notebook_parser.set_headers_visible(True)

        self.column_notebook_parser_position.set_title(_("Line"))
        self.column_notebook_parser_description.set_title(_("Message"))

        self.column_notebook_parser_position.pack_start(
            self.cell_notebook_parser_position, False)
        self.column_notebook_parser_description.pack_start(
            self.cell_notebook_parser_description, True)

        self.column_notebook_parser_position.add_attribute(
            self.cell_notebook_parser_position, "markup", 0)
        self.column_notebook_parser_description.add_attribute(
            self.cell_notebook_parser_description, "markup", 1)

        self.cell_notebook_parser_position.set_alignment(0, .5)
        self.cell_notebook_parser_position.set_padding(6, 4)
        self.cell_notebook_parser_description.set_alignment(0, .5)
        self.cell_notebook_parser_description.set_padding(6, 4)

        self.cell_notebook_parser_description.set_property(
            "ellipsize", Pango.EllipsizeMode.END)

        self.treeview_notebook_parser.append_column(
            self.column_notebook_parser_position)
        self.treeview_notebook_parser.append_column(
            self.column_notebook_parser_description)

        # ------------------------------------
        #   Notebook - Execution
        # ------------------------------------

        self.label_notebook_execution = Gtk.Label()

        self.scroll_notebook_execution = Gtk.ScrolledWindow()

        self.textview_notebook_execution = Gtk.TextView()

        # Properties
        self.label_notebook_execution.set_label(_("Execution"))
        self.label_notebook_execution.set_halign(Gtk.Align.END)

        self.textview_notebook_execution.set_top_margin(6)
        self.textview_notebook_execution.set_left_margin(6)
        self.textview_notebook_execution.set_right_margin(6)
        self.textview_notebook_execution.set_bottom_margin(6)
        self.textview_notebook_execution.set_monospace(True)
        self.textview_notebook_execution.set_editable(False)

        self.textview_notebook_execution.modify_bg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse("black"))
        self.textview_notebook_execution.modify_fg(
            Gtk.StateFlags.NORMAL, Gdk.color_parse("white"))

        # ------------------------------------
        #   Notebook - Execution - Toolbar
        # ------------------------------------

        self.toolbar_execution = Gtk.Toolbar()

        self.toolbar_execution_item_launch = Gtk.ToolItem()
        self.toolbar_execution_item_steps = Gtk.ToolItem()
        self.toolbar_execution_item_entry = Gtk.ToolItem()

        self.toolbar_execution_expand = Gtk.SeparatorToolItem()

        # Properties
        self.toolbar_execution_expand.set_draw(False)
        self.toolbar_execution_expand.set_expand(True)

        # ------------------------------------
        #   Notebook - Execution - Buttons
        # ------------------------------------

        self.toolbar_execution_steps = Gtk.Box()

        self.toolbar_execution_image_next = Gtk.Image()
        self.toolbar_execution_image_current = Gtk.Image()
        self.toolbar_execution_image_previous = Gtk.Image()

        self.toolbar_execution_button_next = Gtk.Button()
        self.toolbar_execution_button_current = Gtk.Button()
        self.toolbar_execution_button_previous = Gtk.Button()

        # Properties
        Gtk.StyleContext.add_class(
            self.toolbar_execution_steps.get_style_context(), "linked")
        self.toolbar_execution_steps.set_spacing(-1)

        self.toolbar_execution_image_next.set_from_icon_name(
            "go-next-symbolic", Gtk.IconSize.BUTTON)
        self.toolbar_execution_image_current.set_from_icon_name(
            "media-playback-start-symbolic", Gtk.IconSize.BUTTON)
        self.toolbar_execution_image_previous.set_from_icon_name(
            "go-previous-symbolic", Gtk.IconSize.BUTTON)

        self.toolbar_execution_button_next.set_image(
            self.toolbar_execution_image_next)
        self.toolbar_execution_button_current.set_image(
            self.toolbar_execution_image_current)
        self.toolbar_execution_button_previous.set_image(
            self.toolbar_execution_image_previous)

        # ------------------------------------
        #   Notebook - Execution - Filters
        # ------------------------------------

        self.toolbar_execution_filter = Gtk.SearchEntry()

        # Properties
        self.toolbar_execution_filter.set_placeholder_text(_("Search..."))


    def __init_packing(self):
        """ Initialize widgets packing in main window
        """

        self.set_titlebar(self.headerbar)

        self.headerbar.pack_start(self.headerbar_button_application)
        self.headerbar.pack_end(self.headerbar_button_menu)

        self.add(self.grid)

        # ------------------------------------
        #   Toolbar
        # ------------------------------------

        self.grid.pack_start(self.toolbar, False, False, 0)
        self.grid.pack_start(self.paned_output, True, True, 0)

        self.toolbar.insert(self.toolbar_item_new, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_open, -1)
        self.toolbar.insert(self.toolbar_item_save, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_copy, -1)
        self.toolbar.insert(self.toolbar_item_cut, -1)
        self.toolbar.insert(self.toolbar_item_paste, -1)
        self.toolbar.insert(Gtk.SeparatorToolItem(), -1)
        self.toolbar.insert(self.toolbar_item_undo, -1)
        self.toolbar.insert(self.toolbar_item_redo, -1)
        self.toolbar.insert(self.toolbar_item_expand, -1)
        self.toolbar.insert(self.toolbar_item_search, -1)

        self.toolbar_item_search.add(self.toolbar_search_entry)

        # ------------------------------------
        #   Sidebar - Symbols
        # ------------------------------------

        self.notebook_sidebar.append_page(
            self.scroll_notebook_symbol, self.label_notebook_symbol)

        self.grid_notebook_symbol.pack_start(
            self.treeview_notebook_symbol, True, True, 0)

        self.scroll_notebook_symbol.add(self.grid_notebook_symbol)

        # ------------------------------------
        #   Sidebar - Data
        # ------------------------------------

        self.notebook_sidebar.append_page(
            self.scroll_notebook_data, self.label_notebook_data)

        self.grid_notebook_data.pack_start(
            self.treeview_notebook_data, True, True, 0)

        self.scroll_notebook_data.add(self.grid_notebook_data)

        # ------------------------------------
        #   Output - Status
        # ------------------------------------

        self.notebook_output.append_page(
            self.scroll_notebook_status, self.label_notebook_status)

        self.scroll_notebook_status.add(self.treeview_notebook_status)

        # ------------------------------------
        #   Output - Parser
        # ------------------------------------

        self.notebook_output.append_page(
            self.scroll_notebook_parser, self.label_notebook_parser)

        self.scroll_notebook_parser.add(self.treeview_notebook_parser)

        # ------------------------------------
        #   Output - Execution
        # ------------------------------------

        self.notebook_output.append_page(
            self.grid_notebook_execution, self.label_notebook_execution)

        self.grid_notebook_execution.pack_start(
            self.toolbar_execution, False, False, 0)
        self.grid_notebook_execution.pack_start(
            self.scroll_notebook_execution, True, True, 0)

        self.scroll_notebook_execution.add(self.textview_notebook_execution)

        # ------------------------------------
        #   Output - Execution - Toolbar
        # ------------------------------------

        self.toolbar_execution.insert(
            self.toolbar_execution_item_launch, -1)
        self.toolbar_execution.insert(
            Gtk.SeparatorToolItem(), -1)
        self.toolbar_execution.insert(
            self.toolbar_execution_item_steps, -1)
        self.toolbar_execution.insert(
            self.toolbar_execution_expand, -1)
        self.toolbar_execution.insert(
            self.toolbar_execution_item_entry, -1)

        self.toolbar_execution_item_launch.add(
            self.toolbar_execution_button_current)
        self.toolbar_execution_item_steps.add(
            self.toolbar_execution_steps)
        self.toolbar_execution_item_entry.add(
            self.toolbar_execution_filter)

        self.toolbar_execution_steps.pack_start(
            self.toolbar_execution_button_previous, False, False, 0)
        self.toolbar_execution_steps.pack_start(
            self.toolbar_execution_button_next, False, False, 0)

        # ------------------------------------
        #   Paned
        # ------------------------------------

        self.paned_sidebar.pack1(self.notebook_sidebar, False, False)
        self.paned_sidebar.pack2(self.notebook_editor, True, False)

        self.paned_output.pack1(self.paned_sidebar, True, False)
        self.paned_output.pack2(self.notebook_output, False, False)


    def __init_signals(self):
        """ Initialize widgets signals
        """

        self.connect(
            "destroy", self.__stop_interface)

        self.toolbar_item_new.connect(
            "clicked", self.__on_new)
        self.toolbar_item_open.connect(
            "clicked", self.__on_open)
        self.toolbar_item_save.connect(
            "clicked", self.__on_save)
        self.toolbar_item_copy.connect(
            "clicked", self.__on_copy)
        self.toolbar_item_cut.connect(
            "clicked", self.__on_cut)
        self.toolbar_item_paste.connect(
            "clicked", self.__on_paste)
        self.toolbar_item_undo.connect(
            "clicked", self.__on_undo)
        self.toolbar_item_redo.connect(
            "clicked", self.__on_redo)

        self.toolbar_search_entry.connect(
            "changed", self.__on_search)
        self.toolbar_search_entry.connect(
            "activate", self.__on_search)
        self.toolbar_search_entry.connect(
            "icon-press", self.__on_search)
        self.toolbar_search_entry.connect(
            "focus-out-event", self.__on_unsearch)

        self.toolbar_execution_button_next.connect(
            "clicked", self.__on_next_step)
        self.toolbar_execution_button_current.connect(
            "clicked", self.__on_execute_program)
        self.toolbar_execution_button_previous.connect(
            "clicked", self.__on_previous_step)

        self.treeview_notebook_symbol.connect(
            "button-press-event", self.__on_selected_symbol)
        self.treeview_notebook_symbol.connect(
            "key-release-event", self.__on_selected_symbol)

        self.cell_notebook_data_value.connect(
            "edited", self.__on_edit_data)

        self.notebook_editor.connect(
            "switch-page", self.on_switch_notebook_tab)


    def __init_file(self, path):
        """ Load a new file into interface

        Parameters
        ----------
        path : str or None
            File path to open
        """

        if path is not None:
            path = abspath(expanduser(path))

            if not exists(path):
                raise OSError(2, _("%s file not exists") % str(path))

            self.__add_status_message(_("Open %s file") % path)

            tab_widget = InterfaceTabEditor(self, path)

            # Store current tab for further uses
            self.__files_storage[path] = tab_widget

        else:
            self.__add_status_message(_("New untitled file"))

            tab_widget = InterfaceTabEditor(self)

        self.notebook_editor.set_show_tabs(True)

        position = self.notebook_editor.append_page(
            tab_widget, tab_widget.get_grid_widget())

        # Connect close button from new tab
        tab_widget.button_close.connect(
            "clicked", self.__on_close_tab, tab_widget)

        self.notebook_editor.set_tab_reorderable(tab_widget, True)

        self.notebook_editor.set_current_page(position)


    def __start_interface(self):
        """ Launch interface
        """

        self.__add_status_message(_("Welcome in ASMx86"))

        # Set toolbar item sensitivity
        self.toolbar_item_undo.set_sensitive(False)
        self.toolbar_item_redo.set_sensitive(False)

        for path in self.__files_path:
            self.__init_file(path)

        self.toolbar_menu_open.show_all()
        self.show_all()

        self.on_update_execution_toolbar()

        Gtk.main()


    def __stop_interface(self, *args):
        """ Save data and stop interface
        """

        for page in range(0, self.notebook_editor.get_n_pages()):
            tab_widget = self.notebook_editor.get_nth_page(page)

            if tab_widget.has_changed():
                path = tab_widget.get_path()

                title = "Untitled"
                if path is not None:
                    title = basename(path)

                dialog = Dialog(self, _("File %s has been modified") % title)

                dialog.set_message(_("Would you want to save it ?"))

                dialog.add_button(_("No"), Gtk.ResponseType.CANCEL)
                dialog.add_button(_("Yes"), Gtk.ResponseType.ACCEPT)

                if dialog.run() == Gtk.ResponseType.ACCEPT:

                    if path is not None:
                        tab_widget.set_buffer(tab_widget.get_buffer(), True)

                    else:
                        self.__on_save()

                dialog.destroy()

        Gtk.main_quit()


    def __add_status_message(self, message):
        """ Add a new message to status tab

        Parameters
        ----------
        message : str
            Message to send
        """

        date = datetime.today().time()

        self.model_notebook_status.append(
            [ date.strftime("%H:%M:%S"), message ])


    def __add_execution_message(self, message):
        """ Add a new message to execution tab

        Parameters
        ----------
        message : tuple
            Message received
        """

        textbuffer = self.textview_notebook_execution.get_buffer()

        text = textbuffer.get_text(
            textbuffer.get_start_iter(), textbuffer.get_end_iter(), False)

        type_message = message[0]

        if type_message == AsmError.InstructionNotImplemented:
            text += "%s\n" % _("Instruction not implemented")

        elif type_message == AsmAction.SendToOutput:
            text += "%s\n" % str(message[1])

        elif type_message == AsmAction.TerminateProgram:
            text += "-- Terminate with status %s --\n" % str(message[1])

        textbuffer.set_text(text)


    def __on_selected_symbol(self, treeview, event):
        """ Select a symbol

        This function occurs when the user select a symbol in sidebar

        Parameters
        ----------
        treeview : Gtk.Treeview
            Object which receive signal
        event : Gdk.EventButton or Gdk.EventKey
            Event which triggered this signal
        """

        available_events = [
            EventType.BUTTON_PRESS,
            EventType._2BUTTON_PRESS,
            EventType._3BUTTON_PRESS
        ]

        treeiter = None

        # ----------------------------
        #   Keyboard
        # ----------------------------

        if event.type == EventType.KEY_RELEASE:
            model, treeiter = treeview.get_selection().get_selected()

        # ----------------------------
        #   Mouse
        # ----------------------------

        elif event.type in available_events and event.button in (1, 3):

            # Get selection from cursor position
            if event.type == EventType.BUTTON_PRESS:
                selection = treeview.get_path_at_pos(int(event.x), int(event.y))

                if selection is not None:
                    model = treeview.get_model()
                    treeiter = model.get_iter(selection[0])

            # Get selection from treeview
            else:
                model, treeiter = treeview.get_selection().get_selected()

        # ----------------------------
        #   Symbol selected
        # ----------------------------

        # Get symbol position
        if treeiter is not None and not model.get_value(treeiter, 4):
            if self.__current_tab is not None:
                self.__current_tab.move_cursor(model.get_value(treeiter, 2) - 1)


    def __on_new(self, widget):
        """ Add a new document

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        self.__init_file(None)


    def __on_open(self, widget):
        """ Launch a dialog to select a file to open

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        self.__previous_buffer = None

        dialog = Gtk.FileChooserDialog(_("Open File"), use_header_bar=True)

        dialog.set_action(Gtk.FileChooserAction.OPEN)
        dialog.set_filter(self.__files_filter)
        dialog.set_select_multiple(False)
        dialog.set_transient_for(self)

        path = expanduser('~')
        if self.__current_tab is not None:
            engine = self.__current_tab.get_engine()

            if engine.get_path() is not None:
                path = basename(engine.get_path())

        dialog.set_current_folder(path)

        dialog.add_button(_("Cancel"), Gtk.ResponseType.CANCEL)
        dialog.add_button(_("Accept"), Gtk.ResponseType.ACCEPT)

        if dialog.run() == Gtk.ResponseType.ACCEPT:
            path = dialog.get_filename()

            if not path in self.__files_storage:
                self.__init_file(path)

        dialog.destroy()


    def __on_save(self, widget=None):
        """ Launch a dialog to select a file to open

        Parameters
        ----------
        widget : Gtk.Widget, optional
            Object which received the signal
        """

        save_file = True

        if self.__current_tab is not None:
            engine = self.__current_tab.get_engine()

            if engine.get_path() is None:
                dialog = Gtk.FileChooserDialog(
                    _("Save File"), use_header_bar=True)

                dialog.set_action(Gtk.FileChooserAction.SAVE)
                dialog.set_filter(self.__files_filter)
                dialog.set_select_multiple(False)
                dialog.set_create_folders(True)
                dialog.set_transient_for(self)

                dialog.set_current_name(_("Untitled.nasm"))
                dialog.set_current_folder(expanduser('~'))

                dialog.add_button(_("Cancel"), Gtk.ResponseType.CANCEL)
                dialog.add_button(_("Accept"), Gtk.ResponseType.ACCEPT)

                if dialog.run() == Gtk.ResponseType.ACCEPT:
                    engine.set_path(dialog.get_filename())

                else:
                    save_file = False

                dialog.destroy()

        if save_file:
            self.__add_status_message(_("Save %s file") % engine.get_path())

            self.__current_tab.set_buffer(self.__current_tab.get_buffer(), True)

            self.on_update_interface()


    def __on_copy(self, widget):
        """ Copy selected text to main clipboard

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.__current_tab.copy()


    def __on_cut(self, widget):
        """ Cut selected text to main clipboard

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.__current_tab.cut()


    def __on_paste(self, widget):
        """ Paste clipboard text to editor

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.__current_tab.paste()


    def __on_undo(self, widget):
        """ Check buffer content when user undo a changes

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.__current_tab.undo()


    def __on_redo(self, widget):
        """ Check buffer content when user redo a changes

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.__current_tab.redo()


    def __on_search(self, widget, pos=None, event=None):
        """ Check buffer content when user redo a changes

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        pos : Gtk.EntryIconPosition, optional
            Position of the clicked icon (Default: None)
        event : Gdk.EventButton or Gdk.EventKey, optional
            Event which triggered this signal (Default: None)
        """

        remove_class = True

        text = self.toolbar_search_entry.get_text()

        # Click on reset icon in search entry
        if type(pos) is Gtk.EntryIconPosition:
            text = str()

        if self.__current_tab is not None:
            status = self.__current_tab.search(text)

            # Color search entry with search result
            if status is None:
                remove_class = False

        else:
            remove_class = False

        # Avoid to have the warning class with empty search
        if len(text) == 0:
            remove_class = True

        if remove_class:
            self.toolbar_search_entry.get_style_context().remove_class(
                "warning")

        else:
            self.toolbar_search_entry.get_style_context().add_class(
                "warning")


    def __on_unsearch(self, widget, event):
        """ Remove search tag when loose focus

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        event : Gdk.EventFocus
            Event which triggered this signal
        """

        if self.__current_tab is not None:
            self.__current_tab.unsearch()

        self.toolbar_search_entry.get_style_context().remove_class("warning")


    def __on_edit_data(self, widget, path, new_value):
        """ Set a new value for a specific register or flag

        Parameters
        ----------
        widget : Gtk.CellRendererText
            Object which received the signal
        path : str
            Path identifying the edited cell
        new_value : str
            Data new value
        """

        if self.__current_tab is not None:
            model = self.treeview_notebook_data.get_model()

            treeiter = model.get_iter(path)

            name = model.get_value(treeiter, 1)

            if self.__current_tab.get_engine().has_register(name):
                register = self.__current_tab.get_engine().get_register(name)
                register.cast(new_value)

                self.__current_tab.get_engine().set_register_value(
                    name, new_value)

                model.set_value(treeiter, 2, str(new_value))


    def __on_execute_program(self, widget):
        """ Execute the program process

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.textview_notebook_execution.get_buffer().set_text(str())

            self.toolbar_execution_button_next.set_sensitive(False)
            self.toolbar_execution_button_current.set_sensitive(False)
            self.toolbar_execution_button_previous.set_sensitive(False)

            self.__current_tab.reset_execution()
            self.__current_tab.set_editable(False)

            main_loop = True

            while main_loop:
                self.__current_tab.next_step()

                self.textview_notebook_execution.get_buffer().set_text(str())

                position = None
                for position, message in self.__current_tab.get_output():

                    if message is not None:
                        self.__add_execution_message(message)

                        # Detect the end of program
                        if message[0] == AsmAction.TerminateProgram:
                            main_loop = False

                if position is not None:
                    self.__current_tab.move_cursor(position)

                self.on_update_data_treeiters()

                # Allow to have a dynamic scrolling
                self.refresh()

                sleep(0.1)

            self.__current_tab.set_editable(True)

        self.on_update_execution_toolbar()


    def __on_next_step(self, widget):
        """ Execute the next step in program process

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.textview_notebook_execution.get_buffer().set_text(str())

            self.__current_tab.next_step()

            position = None
            for position, message in self.__current_tab.get_output():

                if message is not None:
                    self.__add_execution_message(message)

            if position is not None:
                self.__current_tab.move_cursor(position)

            self.on_update_data_treeiters()

        self.on_update_execution_toolbar()


    def __on_previous_step(self, widget):
        """ Execute the previous step in program process

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        """

        if self.__current_tab is not None:
            self.textview_notebook_execution.get_buffer().set_text(str())

            self.__current_tab.previous_step()

            position = None
            for position, message in self.__current_tab.get_output():

                if message is not None:
                    self.__add_execution_message(message)

            if position is not None:
                self.__current_tab.move_cursor(position)

            self.on_update_data_treeiters()

        self.on_update_execution_toolbar()


    def __on_close_tab(self, button, widget):
        """ Close the selected tab

        Parameters
        ----------
        button : Gtk.Button
            The object which received the signal
        widget : Gtk.Widget
            The tab child widget
        """

        # Remove widget from notebook tab
        page = self.notebook_editor.page_num(widget)
        if not page == -1:
            tab_widget = self.notebook_editor.get_nth_page(page)

            path = tab_widget.get_path()

            # Ask user if he wants to save the current file
            if tab_widget.has_changed():

                title = _("Untitled")
                if path is not None:
                    title = basename(path)

                dialog = Dialog(self, _("File %s has been modified") % title)

                dialog.set_message(_("Would you want to save it ?"))

                dialog.add_button(_("No"), Gtk.ResponseType.CANCEL)
                dialog.add_button(_("Yes"), Gtk.ResponseType.ACCEPT)

                if dialog.run() == Gtk.ResponseType.ACCEPT:

                    if path is not None:
                        tab_widget.set_buffer(tab_widget.get_buffer(), True)

                    else:
                        self.__on_save()

                dialog.destroy()

            if path is not None:
                self.__add_status_message(_("Close %s file") % path)

                del self.__files_storage[path]
            else:
                self.__add_status_message(_("Close Untitled file"))

            self.notebook_editor.remove_page(page)

            # Avoid to show notebook tab area when no tab available
            if self.notebook_editor.get_n_pages() == 0:
                self.notebook_editor.set_show_tabs(False)

                self.__current_tab = None

                self.on_update_interface()


    def on_switch_notebook_tab(self, widget, tab, index):
        """ Occurs when the tab has been switch

        Parameters
        ----------
        widget : Gtk.Widget
            Object which received the signal
        tab : Gtk.Widget
            New current page
        index : int
            New page index
        """

        self.__current_tab = tab

        self.on_update_interface()


    def on_update_interface(self, widget=None):
        """ Update interface widgets
        """

        if self.__current_tab is not None:
            self.headerbar.set_title(self.__current_tab.get_label())

            self.toolbar_item_undo.set_sensitive(
                self.__current_tab.can_undo())
            self.toolbar_item_redo.set_sensitive(
                self.__current_tab.can_redo())
            self.toolbar_item_save.set_sensitive(
                self.__current_tab.has_changed())

            self.toolbar_search_entry.set_text(
                self.__current_tab.get_current_search())

            model_symbol, model_data, model_parser = self.__current_tab.update()

            self.treeview_notebook_data.set_model(model_data)
            self.treeview_notebook_symbol.set_model(model_symbol)
            self.treeview_notebook_parser.set_model(model_parser)

            self.treeview_notebook_data.expand_all()
            self.treeview_notebook_symbol.expand_all()

        else:
            self.headerbar.set_title("ASMx86")

            self.toolbar_item_undo.set_sensitive(False)
            self.toolbar_item_redo.set_sensitive(False)
            self.toolbar_item_save.set_sensitive(False)

            self.treeview_notebook_data.set_model(None)
            self.treeview_notebook_symbol.set_model(None)
            self.treeview_notebook_parser.set_model(None)

        self.on_update_execution_toolbar()


    def on_update_execution_toolbar(self, widget=None):
        """ Update interface widgets
        """

        if self.__current_tab is not None:
            self.toolbar_execution_button_current.set_sensitive(True)

            self.toolbar_execution_button_next.set_sensitive(
                self.__current_tab.can_next_step())
            self.toolbar_execution_button_previous.set_sensitive(
                self.__current_tab.can_previous_step())

        else:
            self.toolbar_execution_button_next.set_sensitive(False)
            self.toolbar_execution_button_current.set_sensitive(False)
            self.toolbar_execution_button_previous.set_sensitive(False)


    def on_update_data_treeiters(self):
        """ Update date treeiters with engine values
        """

        if self.__current_tab is not None:
            model = self.treeview_notebook_data.get_model()

            # Update flags
            for treeiter in self.__current_tab.get_flags_iters():
                name = model.get_value(treeiter, 1)

                flag = self.__current_tab.get_engine().get_flag(name)

                model.set_value(treeiter, 2, str(flag))

            # Update registers
            for treeiter in self.__current_tab.get_registers_iters():
                name = model.get_value(treeiter, 1)

                register = self.__current_tab.get_engine().get_register(name)

                model.set_value(treeiter, 2, str(register))


    def refresh(self):
        """ Refresh all pendings event in main interface
        """

        while Gtk.events_pending():
            Gtk.main_iteration()


class Dialog(Gtk.Dialog):

    def __init__(self, parent, title):
        """ Constructor

        Parameters
        ----------
        parent : asmx86.ui.gtk.Interface
            Parent instance
        title : str
            Dialog title
        """

        Gtk.Dialog.__init__(self, title, parent, use_header_bar=True)

        # ------------------------------------
        #   Grid
        # ------------------------------------

        # Add an alias for dialog content area
        self.box = self.get_content_area()

        # Properties
        self.box.set_border_width(18)

        # ------------------------------------
        #   Message
        # ------------------------------------

        self.label = Gtk.Label()

        # Properties
        self.label.set_use_markup(True)

        # ------------------------------------
        #   Integrate widgets
        # ------------------------------------

        self.box.pack_start(self.label, True, True, 0)

        self.box.show_all()


    def set_message(self, text):
        """ Set dialog internal message

        Parameters
        ----------
        text : str
            Message to write
        """

        self.label.set_markup(text)
