# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.ui.gtk import *

from asmx86.ui.utils import *

# ------------------------------------------------------------------------------
#   Modules - Translation
# ------------------------------------------------------------------------------

from gettext import gettext as _
from gettext import textdomain
from gettext import bindtextdomain

bindtextdomain("asmx86", get_data("i18n"))
textdomain("asmx86")

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class InterfaceTabEditor(Gtk.ScrolledWindow):

    def __init__(self, interface, path=None):
        """ Constructor

        Parameters
        ----------
        interface : asmx86.ui.gtk.Interface
            Interface instance
        path : str, optional
            Current tab file path
        """

        Gtk.ScrolledWindow.__init__(self)

        # ------------------------------------
        #   Variables
        # ------------------------------------

        self.interface = interface

        # Buffer storage to detect file modification
        self.__previous_buffer = None

        # Models root iter storage
        self.__storage_iters = dict()

        # Editor search metadata
        self.__first_iter = None
        self.__current_iter = None
        self.__current_search = str()
        self.__modified_buffer = False

        # Registers and flags storages
        self.__data_storage = {
            "flags": list(),
            "registers": list(),
        }

        # ------------------------------------
        #   Initialize engine
        # ------------------------------------

        self.__engine = Engine(path, debug=self.interface.debug)

        self.label = _("Untitled")
        if path is not None:
            self.label = basename(path)

        # ------------------------------------
        #   Prepare interface
        # ------------------------------------

        # Init widgets
        self.__init_widgets()

        # Init packing
        self.__init_packing()

        # Init signals
        self.__init_signals()

        # Init interface
        self.__init_interface()


    def __init_widgets(self):
        """ Initialize interface widgets
        """

        # ------------------------------------
        #   Tab internal widget
        # ------------------------------------

        self.__grid_close = Gtk.Box()

        self.__label_close = Gtk.Label()

        self.__image_close = Gtk.Image()
        self.button_close = Gtk.Button()

        # Properties
        self.__grid_close.set_spacing(4)
        self.__grid_close.set_orientation(Gtk.Orientation.HORIZONTAL)

        self.__label_close.set_halign(Gtk.Align.START)
        self.__label_close.set_markup(self.label)
        self.__label_close.set_use_markup(True)

        self.__image_close.set_from_icon_name(
            "window-close", Gtk.IconSize.BUTTON)

        self.button_close.set_image(self.__image_close)
        self.button_close.set_relief(Gtk.ReliefStyle.NONE)

        # ------------------------------------
        #   Treeviews model
        # ------------------------------------

        self.__model_symbol = Gtk.TreeStore(str, str, int, bool, bool)

        self.__model_data = Gtk.TreeStore(str, str, str, bool)

        self.__model_parser = Gtk.ListStore(str, str)
        self.__filter_parser = self.__model_parser.filter_new()

        # Properties
        self.__model_symbol.set_sort_column_id(2, Gtk.SortType.ASCENDING)
        self.__model_parser.set_sort_column_id(0, Gtk.SortType.ASCENDING)

        # ------------------------------------
        #   Editor
        # ------------------------------------

        self.editor = GtkSource.View()
        self.editor_buffer = GtkSource.Buffer()

        self.editor_language = GtkSource.LanguageManager()
        self.editor_style = GtkSource.StyleSchemeManager()

        # Properties
        self.editor.set_top_margin(6)
        self.editor.set_left_margin(12)
        self.editor.set_right_margin(12)
        self.editor.set_bottom_margin(6)

        self.editor.set_tab_width(4)
        self.editor.set_insert_spaces_instead_of_tabs(True)

        self.editor.set_show_right_margin(True)
        self.editor.set_right_margin_position(80)

        self.editor.set_monospace(True)
        self.editor.set_auto_indent(True)
        self.editor.set_indent_on_tab(True)
        self.editor.set_smart_backspace(True)
        self.editor.set_show_line_numbers(True)
        self.editor.set_highlight_current_line(True)

        self.editor.set_buffer(self.editor_buffer)

        self.editor_buffer.set_style_scheme(
            self.editor_style.get_scheme("classic"))

        self.editor_buffer.set_language(
            self.editor_language.get_language("nasm"))

        self.editor_tag = self.editor_buffer.create_tag("found",
            background="white", foreground="black")


    def __init_packing(self):
        """ Initialize widgets packing in main window
        """

        self.__grid_close.pack_start(self.__label_close, True, True, 0)
        self.__grid_close.pack_start(self.button_close, False, False, 0)

        self.add(self.editor)


    def __init_signals(self):
        """ Initialize widgets signals
        """

        self.editor.connect(
            "redo", self.check_buffer)
        self.editor.connect(
            "undo", self.check_buffer)

        self.editor_buffer.connect(
            "changed", self.check_buffer)


    def __init_interface(self):
        """ Initialize interface
        """

        if self.__engine.get_path() is not None:
            self.__output = self.__engine.parse()

            self.set_buffer('\n'.join(self.__engine.get_buffer()))

            self.editor_buffer.set_modified(False)

        self.__init_models()

        # Place the cursor on the first line during opening
        self.move_cursor(0)

        self.__grid_close.show_all()
        self.show_all()

        self.editor.grab_focus()


    def __init_models(self):
        """ Iniatialize models treeviews
        """

        self.__model_data.clear()
        self.__model_symbol.clear()
        self.__storage_iters.clear()

        models = OrderedDict({
            self.__model_symbol: self.interface.models_data["symbols"],
            self.__model_data: self.interface.models_data["data"]
        })

        for model, data in models.items():

            for label, icon in data.items():

                if model == self.__model_symbol:
                    row = [ icon, "<b>%s</b>" % label, int(), False, True ]

                elif model == self.__model_data:
                    row = [ icon, "<b>%s</b>" % label, str(), True ]

                self.__storage_iters[label] = model.append(None, row)

        # ------------------------------------
        #   Registers
        # ------------------------------------

        self.__data_storage["registers"] = list()

        root = self.__storage_iters[_("Registers")]

        iters = OrderedDict(dict())
        for size in self.__engine.get_registers_arch():
            iters[size] = self.__model_data.append(root,
                [ None, "<b>%d bits</b>" % size, str(), False ])

        for register in self.__engine.get_registers():
            treeiter = iters[register.size * 8]

            if treeiter is not None:
                row = self.__model_data.append(treeiter,
                    [ None, register.name, str(register.value), False ])

                self.__data_storage["registers"].append(row)

        # ------------------------------------
        #   Flags
        # ------------------------------------

        self.__data_storage["flags"] = list()

        root = self.__storage_iters[_("Flags")]

        for flag in self.__engine.get_flags():
            row = self.__model_data.append(root,
                [ None, flag.name, str(flag.value), False ])

            self.__data_storage["flags"].append(row)


    def __init_search(self):
        """ Initialize search from search entry
        """

        if self.__current_iter is not None:
            self.__unmark_search()

        self.__current_iter = \
            self.editor_buffer.get_start_iter().forward_search(
            self.__current_search, 0, self.editor_buffer.get_end_iter())

        self.__mark_search()


    def __next_search(self, backward):
        """ Move between search results

        Parameters
        ----------
        backward : bool
            If True, use backward search instead of forward

        Todo
        ----
            * Implements backward search
        """

        if self.__current_iter is not None:
            self.__unmark_search()

            self.__current_iter = self.__current_iter[1].forward_search(
                self.__current_search, 0, self.editor_buffer.get_end_iter())

            if self.__current_iter is None:
                self.__current_iter = \
                    self.editor_buffer.get_start_iter().forward_search(
                    self.__current_search, 0, self.editor_buffer.get_end_iter())

            self.__mark_search()


    def __mark_search(self):
        """ Mark the next found text iter
        """

        if self.__current_iter is not None:
            self.editor_buffer.apply_tag(
                self.editor_tag, self.__current_iter[0], self.__current_iter[1])

            self.editor.scroll_to_iter(
                self.__current_iter[0], .25, False, .0, .0)


    def __unmark_search(self):
        """ Remove the previous used tag
        """

        if self.__current_iter is not None:
            self.editor_buffer.remove_tag(
                self.editor_tag, self.__current_iter[0], self.__current_iter[1])


    def check_buffer(self, widget=None, data=None):
        """ Check buffer status

        Parameters
        ----------
        widget : Gtk.Widget or None, optional
            Object which received the signal (Default: None)
        data : object or None, optional
            Available parameters (Default: None)
        """

        if not self.__previous_buffer == self.get_buffer() or self.can_undo():
            self.editor_buffer.set_modified(True)

        self.__output = self.__engine.parse(self.get_buffer().split('\n'))

        self.__current_iter = None

        self.interface.on_update_interface()


    def has_changed(self):
        """ Retrieve buffer change status

        Returns
        -------
        bool
            Change status
        """

        return self.editor_buffer.get_modified()


    def can_redo(self):
        """ Retrieve redo status

        Returns
        -------
        bool
            Redo status
        """

        return self.editor_buffer.can_redo()


    def can_undo(self):
        """ Retrieve undo status

        Returns
        -------
        bool
            Undo status
        """

        return self.editor_buffer.can_undo()


    def can_execute(self):
        """ Check program execution status

        Returns
        -------
        bool
            Program status
        """

        return self.__engine.can_execute()


    def can_next_step(self):
        """ Check next step status

        Returns
        -------
        bool
            Next step status
        """

        return self.__engine.can_next_step()


    def can_previous_step(self):
        """ Check previous step status

        Returns
        -------
        bool
            Previous step status
        """

        return self.__engine.can_previous_step()


    def redo(self):
        """ Check buffer content when user redo a changes
        """

        if self.editor_buffer.can_redo():
            self.editor_buffer.redo()

            self.check_buffer()


    def undo(self):
        """ Check buffer content when user undo a changes
        """

        if self.editor_buffer.can_undo():
            self.editor_buffer.undo()

            self.check_buffer()


    def copy(self):
        """ Copy selected buffer text to main clipboard
        """

        if self.editor_buffer.get_has_selection():
            self.editor_buffer.copy_clipboard(self.interface.clipboard)

            self.check_buffer()


    def cut(self):
        """ Cut selected buffer text to main clipboard
        """

        if self.editor_buffer.get_has_selection():
            self.editor_buffer.cut_clipboard(self.interface.clipboard, True)

            self.check_buffer()


    def paste(self):
        """ Cut selected buffer text to main clipboard
        """

        self.editor_buffer.paste_clipboard(self.interface.clipboard, None, True)

        self.check_buffer()


    def search(self, text, backward=False):
        """ Search entry value in text buffer

        Parameters
        ----------
        text : str
            Text to search in text buffer
        backward : bool, optional
            If True, use backward search instead of forward (Default: False)
        """

        if len(text) > 0:

            if self.__current_iter is None or not text == self.__current_search:
                self.__current_search = text

                self.__init_search()

            else:
                self.__next_search(backward)

        else:
            self.__current_search = str()

            self.__unmark_search()

        return self.__current_iter


    def unsearch(self):
        """ Remove tag in editor buffer
        """

        self.__unmark_search()


    def next_step(self):
        """ Execute next step from program

        Returns
        -------
        object or None
            New instruction
        """

        return self.__engine.next_step()


    def previous_step(self):
        """ Execute previous step from program

        Returns
        -------
        object or None
            New instruction
        """

        return self.__engine.previous_step()


    def update(self):
        """ Update data by parsing text buffer
        """

        self.__output = self.__engine.parse(self.get_buffer().split('\n'))

        program = self.__engine.get_program()

        # Retrieve available programs
        subprograms = program.get_macros()
        subprograms.insert(0, program)

        # Remove previous data
        for label in self.interface.models_data["symbols"].keys():
            root = self.__storage_iters[label]

            # Remove previous child from specified iter
            while self.__model_symbol.iter_has_child(root):
                self.__model_symbol.remove(
                    self.__model_symbol.iter_children(root))

        # Retrieve parsing informations
        for program in subprograms:

            root = self.__storage_iters[_("Modules")]
            for data in program.get_externals():
                self.__model_symbol.append(root, [
                    None, data.name, data.position + 1, True, False ])

            root = self.__storage_iters[_("Variables")]
            for data in program.get_variables():
                self.__model_symbol.append(root, [
                    None, data.name, data.position + 1, True, False ])

            root = self.__storage_iters[_("Labels")]
            for section in program.get_sections():
                for label in section.get_labels():
                    self.__model_symbol.append(root, [
                        None, label.name, label.position + 1, True, False ])

        # Retrieve parsing errors
        self.__model_parser.clear()
        for output in self.__output:
            position, message = get_parser_messages(*output)

            if position is None:
                position = str()

            self.__model_parser.append([ str(position), message ])

        return (self.__model_symbol, self.__model_data, self.__filter_parser)


    def reset_execution(self):
        """ Reset execution process
        """

        self.__engine.reset_execution()


    def move_cursor(self, position):
        """ Move editor cursor to a specific position

        Parameters
        ----------
        position : int
            Line position
        """

        textiter = self.editor_buffer.get_iter_at_line(position)

        # Move text cursor to symbol position
        self.editor_buffer.place_cursor(textiter)

        # Scroll editor buffer to text cursor
        self.editor.scroll_to_iter(textiter, 0.4, True, 0.0, 0.5)


    def get_buffer(self):
        """ Retrieve text buffer

        Returns
        -------
        str
            Text buffer
        """

        return self.editor_buffer.get_text(
            self.editor_buffer.get_start_iter(),
            self.editor_buffer.get_end_iter(), False)


    def get_current_step(self):
        """ Retrieve current instruction

        Returns
        -------
        object or None
            Current instruction
        """

        return self.__engine.current_step()


    def get_current_search(self):
        """ Retrieve current search text

        Returns
        -------
        str
            Search text
        """

        return self.__current_search


    def get_engine(self):
        """ Retrieve engine instance

        Returns
        -------
        asmx86.lib.engine.Engine
            Engine instance
        """

        return self.__engine


    def get_output(self):
        """ Retrieve engine execution output

        Returns
        -------
        list
            Output as string list
        """

        return self.__engine.get_execution_output()


    def get_flags_iters(self):
        """ Retrieve flags treeiter

        Returns
        -------
        list
            Treeiter list
        """

        return self.__data_storage["flags"]


    def get_grid_widget(self):
        """ Retrieve tab grid widget

        Returns
        -------
        Gtk.Box
            Grid widget
        """

        return self.__grid_close


    def get_label(self):
        """ Retrieve label text

        Returns
        -------
        str
            Label text
        """

        return self.__label_close.get_text()


    def get_path(self):
        """ Retrieve file path

        Returns
        -------
        str or None
            File path
        """

        return self.__engine.get_path()


    def get_registers_iters(self):
        """ Retrieve registers treeiter

        Returns
        -------
        list
            Treeiter list
        """

        return self.__data_storage["registers"]


    def set_buffer(self, textbuffer, save=False):
        """ Retrieve text buffer

        Parameters
        ----------
        textbuffer : str
            New text buffer
        save : bool, optional
            If the engine need to save the file (Default: False)
        """

        self.__previous_buffer = textbuffer

        if not save:
            self.editor_buffer.set_text(textbuffer)

            # Remove undo stack from GtkSource.Buffer
            if type(self.editor_buffer) is not Gtk.TextBuffer:
                self.editor_buffer.set_undo_manager(None)

        else:
            self.set_label(basename(self.__engine.get_path()))

            self.__engine.save(textbuffer)

            self.editor_buffer.set_modified(False)


    def set_label(self, text):
        """ Set label text

        Parameters
        ----------
        str
            New text
        """

        self.__label_close.set_markup(text)


    def set_editable(self, status):
        """ Set the editable status of textview

        Parameters
        ----------
        status : bool
            Editable status
        """

        self.editor.set_editable(status)
