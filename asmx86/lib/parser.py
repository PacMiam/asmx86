# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser
from os.path import join as path_join

# Logging
import logging
from logging.config import fileConfig

# Regex
from re import IGNORECASE
from re import compile as re_compile

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.lib.flag import AsmError

from asmx86.lib.structure import AsmProgram
from asmx86.lib.structure import AsmSection

from asmx86.lib.directive import AsmLabel
from asmx86.lib.directive import AsmModule
from asmx86.lib.directive import AsmVariable
from asmx86.lib.directive import AsmInstruction

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Parser(object):

    def __init__(self, path=None, debug=False):
        """ Constructor

        Parameters
        ----------
        path : str, optional
            File path string (Default: None)
        debug : bool, optional
            Set debug flag (Default: False)

        Raises
        ------
        IOError
            if file path not exists on filesystem
        """

        if path is not None and not exists(expanduser(path)):
            raise OSError(2, "%s file not exists" % str(path))

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        # File path
        self.__path = path
        # File buffer
        self.__buffer = list()

        # Logging module
        self.__logger = logging.getLogger("asmx86")

        # Debug flag
        self.debug = debug

        # ----------------------------------------
        #   Regex
        # ----------------------------------------

        self.__regex = [
            {
                "pattern": re_compile(
                    r"extern\s+(\w+)", IGNORECASE),
                "method": self.__parse_external
            },
            {
                "pattern": re_compile(
                    r"global\s+(\w+)", IGNORECASE),
                "method": self.__parse_global
            },
            {
                "pattern": re_compile(
                    r"section\s+\.(\w+)", IGNORECASE),
                "method": self.__parse_section
            },
            {
                "pattern": re_compile(
                    r"(\w+)\s*\:\s*(\w+)\s+(.+)", IGNORECASE),
                "method": self.__parse_variable
            },
            {
                "pattern": re_compile(
                    r"(\w+)\:$", IGNORECASE),
                "method": self.__parse_label
            },
            {
                "pattern": re_compile(
                    r"\.?(\w+)\s+(d\w+)\s+(.+)", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"(\w+)$", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"(\w+)\s+(\w+)$", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"(\w+)\s+(\".+\")$", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"(\w+)\s+(\[?.+\]?)\s*,+\s*(\[?.+\]?)$", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"(\w+)\s+(\[?.+\]?)\s+(\[?.+\]?)$", IGNORECASE),
                "method": self.__parse_instruction
            },
            {
                "pattern": re_compile(
                    r"%endmacro", IGNORECASE),
                "method": self.__close_macro
            },
            {
                "pattern": re_compile(
                    r"%macro\s+(.+)\s+(.*)", IGNORECASE),
                "method": self.__parse_macro
            }
        ]

        # ----------------------------------------
        #   Engine logger
        # ----------------------------------------

        # Define log path with a global variable
        logging.log_path = expanduser(path_join("asmx86.log"))

        # Generate logger from log.conf
        fileConfig(expanduser(path_join("conf", "log.conf")))

        self.logger = logging.getLogger("asmx86")

        if not self.debug:
            self.logger.setLevel(logging.INFO)


    def __reset(self):
        """ Reset parser variables and cache storages
        """

        self.__buffer.clear()

        self.__program = AsmProgram()

        self.__current_line = int()

        self.__current_program = self.__program
        self.__current_section = None


    # ----------------------------------------
    #   Methods
    # ----------------------------------------

    def __parse_line(self, line):
        """ Parse specified line to retrieve usefull informations

        Parameters
        ----------
        line : str
            The line to parse

        Returns
        -------
        tuple or None
            Output result
        """

        # Avoid to parse commented lines
        if not line.startswith(';'):

            # Remove useless comments
            if ';' in line:
                line = line[:line.index(';')].strip()

            # Check pattern
            for data in self.__regex:
                result = data["pattern"].match(line)

                if result is not None:
                    try:
                        method = data["method"]

                        if method is not None:
                            if len(result.groups()) > 0:
                                return method(*result.groups())

                            else:
                                return method()

                        # No need to go further
                        return None

                    except Exception as error:
                        return error


    def __parse_external(self, name):
        """ Parse an extern directive

        Parameters
        ----------
        name : str
            Directive name
        """

        try:
            self.__current_program.add_external(
                AsmModule(name, self.__current_line))

            return None

        except KeyError as error:
            return (self.__current_line, AsmError.AlreadyExistsExternal, name)


    def __parse_global(self, name):
        """ Parse a global directive

        Parameters
        ----------
        name : str
            Directive name
        """

        try:
            self.__current_program.add_global(
                AsmModule(name, self.__current_line))

            return None

        except KeyError as error:
            return (self.__current_line, AsmError.AlreadyExistsGlobal, name)


    def __parse_instruction(self, *data):
        """ Parse an instruction

        Parameters
        ----------
        data : tuple
            Instruction parameters
        """

        if self.__current_section is not None:
            try:
                parameters = list(filter(None, data))

                if self.__current_section.name.lower() == "data":
                    self.__current_program.add_variable(AsmVariable(
                        *parameters, position=self.__current_line))

                else:
                    self.__current_section.add_directive(AsmInstruction(
                        *parameters, position=self.__current_line))

            except Exception as error:
                if self.__current_section.name.lower() == "data":
                    return (self.__current_line, AsmError.WrongVariableSyntax)

                return (self.__current_line, AsmError.WrongInstructionSyntax)


    def __parse_label(self, name):
        """ Parse a label

        Parameters
        ----------
        name : str
            Label name
        """

        if self.__current_section is not None:

            try:
                self.__current_section.add_directive(
                    AsmLabel(name, self.__current_line))

            except Exception as error:
                return (self.__current_line, AsmError.WrongLabelSyntax)


    def __parse_macro(self, name, length):
        """ Parse a macro

        Parameters
        ----------
        name : str
            Macro name
        length : str
            Macro parameters length
        """

        self.__current_program = AsmProgram(self.__program)

        self.__program.add_macro(name, self.__current_program)


    def __parse_section(self, name):
        """ Parse a section

        Parameters
        ----------
        name : str
            Section name
        """

        self.__current_section = AsmSection(name, self.__current_line)

        self.__current_program.add_section(self.__current_section)


    def __parse_variable(self, name, structure, value):
        """ Parse a variable

        Parameters
        ----------
        name : str
            Variable name
        structure : str
            Variable structure type
        value : str
            Variable value
        """

        try:
            self.__current_program.add_variable(
                AsmVariable(name, structure, value, self.__current_line))

        except Exception as error:
            return (self.__current_line, AsmError.WrongVariableSyntax)


    def __close_macro(self):
        """ Close macro block
        """

        self.__current_program = self.__program


    def __open_document(self):
        """ Open document and retrieve text buffer

        Returns
        -------
        list
            Text buffer

        Raises
        ------
        IOError
            if file path not exists on filesystem
        """

        textbuffer = list()

        if self.__path is not None:

            if not exists(self.__path):
                raise OSError(2, "%s file not exists" % str(path))

            self.logger.info("Parse %s file" % basename(self.__path))

            with open(expanduser(self.__path), 'r') as pipe:

                for line in pipe.readlines():
                    textbuffer.append(line[:-1])

        return textbuffer


    def parse(self, textbuffer=None):
        """ Parse file to retrieve usefull informations

        Parameters
        ----------
        textbuffer : list, optional
            Custom text buffer to parse (Default: None)

        Returns
        -------
        list
            Parser output message list
        """

        output = list()

        # Reset parser
        self.__reset()

        # Read file
        if textbuffer is None:
            textbuffer = self.__open_document()

        for line in textbuffer:

            # Put this line to text buffer
            self.__buffer.append(line)

            # Avoid to parse empty lines
            if len(line) > 0:
                result = self.__parse_line(line.strip())

                if result is not None:
                    output.append(result)

            self.__current_line += 1

        return output


    # ----------------------------------------
    #   Getter/Setter
    # ----------------------------------------

    def has_global(self, name):
        """ Check if a specific global is available

        Parameters
        ----------
        name : str
            Global name to check

        Returns
        -------
        bool
            Available status
        """

        return name in self.__globals


    def get_buffer(self):
        """ Retrieve file buffer

        Returns
        -------
        list
            File buffer as string list
        """

        return self.__buffer


    def get_path(self):
        """ Retrieve current file path

        Returns
        -------
        str
            File path
        """

        if self.__path is not None:
            return expanduser(self.__path)

        return None


    def get_program(self):
        """ Retrieve main program

        Returns
        -------
        AsmProgram
            Main program instance
        """

        return self.__program


    def set_path(self, path):
        """ Set file path

        Parameters
        ----------
        path : str
            The new file path
        """

        self.__path = expanduser(path)
