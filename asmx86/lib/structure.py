# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.lib.directive import AsmLabel

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class AsmProgram(object):

    def __init__(self, parent=None):
        """ Constructor

        Parameters
        ----------
        parent : AsmProgram or None, optional
            Parent for current program (Default: None)
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__parent = parent

        # Program submacros
        self.__macros = dict()

        # Program sections
        self.__sections = dict()

        # Program variables
        self.__variables = dict()

        # Program directives
        self.__globals = dict()
        self.__externals = dict()


    @property
    def parent(self):
        """ Retrieve program parent

        Returns
        -------
        AsmProgram or None
            Program parent
        """

        return self.__parent


    def has_data_section(self):
        """ Check if program has a data section

        Returns
        -------
        bool
            True if program has a data section, False otherwhise
        """

        return "data" in list(self.__sections.keys())


    def has_text_section(self):
        """ Check if program has a text section

        Returns
        -------
        bool
            True if program has a text section, False otherwhise
        """

        return "text" in list(self.__sections.keys())


    def has_external(self, name):
        """ Check if program contains a specific external directive

        Parameters
        ----------
        name : str
            External directive name

        Returns
        -------
        bool
            True if program has the specified external directive, False
            otherwhise
        """

        return name in self.__externals.keys()


    def has_global(self, name):
        """ Check if program contains a specific global directive

        Parameters
        ----------
        name : str
            Global directive name

        Returns
        -------
        bool
            True if program has the specified global directive, False otherwhise
        """

        return name in self.__globals.keys()


    def has_variable(self, name):
        """ Check if program contains a specific variable

        Parameters
        ----------
        name : str
            Variable name

        Returns
        -------
        bool
            True if program has the specified variable, False otherwhise
        """

        return name in self.__variables.keys()


    def add_external(self, module):
        """ Add a new external directive to program

        Parameters
        ----------
        module : AsmModule
            New external directive

        Raises
        ------
        KeyError
            If the external directive name already exists in program
        """

        if module.name.lower() in self.__externals.keys():
            raise KeyError(
                "%s already exists in program externals" % module.name)

        self.__externals[module.name.lower()] = module


    def add_global(self, module):
        """ Add a new global directive to program

        Parameters
        ----------
        module : AsmModule
            New global directive

        Raises
        ------
        KeyError
            If the global directive name already exists in program
        """

        if module.name.lower() in self.__globals.keys():
            raise KeyError(
                "%s already exists in program globals" % module.name)

        self.__globals[module.name.lower()] = module


    def add_macro(self, name, macro):
        """ Add a new macro to program

        Parameters
        ----------
        name : str
            Macro name
        macro : AsmProgram
            Macro program instance

        Raises
        ------
        KeyError
            If the macro name already exists in program
        """

        if name.lower() in self.__macros.keys():
            raise KeyError("%s already exists in program macros" % name)

        self.__macros[name.lower()] = macro


    def add_section(self, section):
        """ Add a new section to program

        Parameters
        ----------
        section : AsmSection
            New section to add into program

        Raises
        ------
        KeyError
            If the section name already exists in program
        """

        if section.name.lower() in self.__sections.keys():
            raise KeyError(
                "%s already exists in program sections" % section.name)

        self.__sections[section.name.lower()] = section


    def add_variable(self, variable):
        """ Add a new variable to program

        Parameters
        ----------
        variable : AsmVariable
            New variable to add into program

        Raises
        ------
        KeyError
            If the variable name already exists in program
        """

        if variable.name.lower() in self.__variables.keys():
            raise KeyError(
                "%s already exists in program variables" % variable.name)

        self.__variables[variable.name.lower()] = variable


    def get_externals(self):
        """ Retrieve external modules

        Returns
        -------
        list
            String list
        """

        return list(self.__externals.values())


    def get_globals(self):
        """ Retrieve global modules

        Returns
        -------
        list
            String list
        """

        return list(self.__globals.values())


    def get_macro(self, name):
        """ Retrieve a macro from his name

        Parameters
        ----------
        name : str
            Macro name

        Returns
        -------
        AsmProgram / None
            Macro instance or None if not available
        """

        if name in self.__macros.keys():
            return self.__macros[name]

        return None


    def get_macros(self):
        """ Retrieve macro instances

        Returns
        -------
        list
            AsmProgram list
        """

        return list(self.__macros.values())


    def get_section(self, name):
        """ Retrieve a section from his name

        Parameters
        ----------
        name : str
            Section name

        Returns
        -------
        AsmSection / None
            Section instance or None if not available
        """

        if name in self.__sections.keys():
            return self.__sections[name]

        return None


    def get_sections(self):
        """ Retrieve section directives

        Returns
        -------
        list
            AsmSection list
        """

        return list(self.__sections.values())


    def get_variable(self, name):
        """ Retrieve a variable from his name

        Parameters
        ----------
        name : str
            Variable name

        Returns
        -------
        AsmVariable / None
            Variable instance or None if not available
        """

        if name in self.__variables.keys():
            return self.__variables[name]

        return None


    def get_variables(self):
        """ Retrieve variables directives

        Returns
        -------
        list
            AsmVariable list
        """

        return list(self.__variables.values())


class AsmSection(object):

    def __init__(self, name, position):
        """ Constructor

        Parameters
        ----------
        name : str
            Section name
        position : int
            Section position
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = name
        self.position = position

        self.__buffer = list()


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        return self.name


    def __getitem__(self, index):
        """ Retrieve a specific line from section buffer

        Parameters
        ----------
        index : int
            Buffer line position

        Returns
        -------
        object
            Buffer content at index position

        Raises
        ------
        IndexError
            If index is not in buffer range
        """

        if not index in range(0, len(self.__buffer)):
            raise IndexError(
                "Cannot access to position %d in buffer content" % index)

        return self.__buffer[index]


    def __len__(self):
        """ Retrieve buffer length

        Returns
        -------
        int
            Buffer lines length
        """

        return int(len(self.__buffer))


    def get(self):
        """ Retrieve section buffer

        Returns
        -------
        list
            Section buffer list
        """

        return self.__buffer


    def get_labels(self):
        """ Retrieve labels from section

        Returns
        -------
        list
            AsmLabel list
        """

        data = list()

        for element in self.__buffer:
            if type(element) is AsmLabel:
                data.append(element)

        return data


    def add_directive(self, data):
        """ Add a new data to section buffer

        Parameters
        ----------
        data : object
            The new object to add in section buffer
        """

        self.__buffer.append(data)


    def has_directive(self, name):
        """ Check if section contains a specific directive

        Parameters
        ----------
        name : str
            Directive name

        Returns
        -------
        list or None
            Directives list or None if not available
        """

        result = list()

        for directive in self.__buffer:

            if directive.name == name:
                result.append(directive)

        if len(result) > 0:
            return result

        return None
