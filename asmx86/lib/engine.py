# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Configuration
from configparser import ConfigParser

# Copy
from copy import deepcopy

# Filesystem
from os.path import exists
from os.path import basename
from os.path import expanduser
from os.path import join as path_join

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86.lib.parser import Parser

from asmx86.lib.flag import *

from asmx86.lib.directive import *

from asmx86.lib.instructions import Instructions

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Engine(Parser):

    def __init__(self, path=None, debug=False):
        """ Constructor

        Parameters
        ----------
        path : str or None, optional
            File path string (Default: None)
        debug : bool, optional
            Set debug flag (Default: False)
        """

        Parser.__init__(self, path, debug)

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.__execution_step = int()

        self.__execution_stoped = False
        self.__execution_started = False

        self.__current_section = None

        self.program = None

        # Store x86 directives
        self.__flags = dict()
        self.__registers = dict()
        self.__variables = dict()

        # Store directives instances in execution process
        self.__execution_buffer = list()

        # Store available registers arch
        self.__registers_arch = list()

        # Store available instructions
        self.__instructions_storage = dict()

        # ----------------------------------------
        #   Engine configuration
        # ----------------------------------------

        self.__config_path = expanduser(path_join("conf", "engine.conf"))

        if not exists(self.__config_path):
            raise OSError(
                2, "Cannot access to %s" % basename(self.__config_path))

        self.config = ConfigParser()
        self.config.read(self.__config_path)

        # ----------------------------------------
        #   Engine instructions
        # ----------------------------------------

        for key in Instructions.__dict__.keys():
            if not key.startswith("__") and not key.endswith("__"):
                self.__instructions_storage[key] = Instructions.__dict__[key]

        # ----------------------------------------
        #   Engine data
        # ----------------------------------------

        for register, size in self.config.items("register"):

            # Check if the register type has already been stored
            if not int(size) in self.__registers_arch:
                self.__registers_arch.append(int(size))

            # Retrieve stack size
            size, rest = divmod(int(size), 8)

            # Create and store register
            self.__registers[register] = AsmRegister(register, size)

        for flag, size in self.config.items("flag"):

            # Retrieve stack size
            size, rest = divmod(int(size), 8)

            # Create and store flag
            self.__flags[flag] = AsmRegister(flag, size)

        self.__execution_buffer.append({
            "output": list(),
            "flags": deepcopy(self.__flags),
            "registers": deepcopy(self.__registers),
        })


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        text = list()

        text.append("File: %s" % self.get_path())
        text.append(" -> %-13s: %d" % ("Macros", len(self.get_macros())))
        text.append(" -> %-13s: %d" % ("Sections", len(self.get_sections())))
        text.append(" -> %-13s: %d" % ("Buffer lines", len(self.get_buffer())))

        return '\n'.join(text)


    def parse(self, textbuffer=None):
        """ Parse file to retrieve usefull informations

        Parameters
        ----------
        textbuffer : list, optional
            Custom text buffer to parse (Default: None)

        Returns
        -------
        list
            Parser output message list
        """

        # Reset engine variables and directives
        self.reset_execution()

        self.__variables.clear()

        # Parse current file to retrieve informations
        output = Parser.parse(self, textbuffer)

        # Retrieve main program instance
        self.program = self.get_program()

        # ----------------------------------------
        #   Data block
        # ----------------------------------------

        if self.program.has_data_section():

            # Store variables for further access
            for element in self.program.get_section("data"):

                if type(element) is AsmVariable:
                    self.__variables[element.name] = element

        else:
            output.append((None, AsmError.MissingSectionData))

        # ----------------------------------------
        #   Text block
        # ----------------------------------------

        if self.program.has_text_section():

            # Store text section as default
            self.__current_section = self.program.get_section("text")

        else:
            output.append((None, AsmError.MissingSectionText))

        # ----------------------------------------
        #   Debug status
        # ----------------------------------------

        if self.debug and textbuffer is None:

            if len(self.get_registers()) > 0:
                self.logger.debug(
                    "Found %d register(s)" % len(self.get_registers()))

            if len(self.get_flags()) > 0:
                self.logger.debug(
                    "Found %d flag(s)" % len(self.get_flags()))

            if len(self.program.get_macros()) > 0:
                self.logger.debug(
                    "Found %d macro(s)" % len(self.program.get_macros()))

            if len(self.program.get_sections()) > 0:
                self.logger.debug(
                    "Found %d section(s)" % len(self.program.get_sections()))

            if len(self.program.get_variables()) > 0:
                self.logger.debug(
                    "Found %d variable(s)" % len(self.program.get_variables()))

        return output


    def current_step(self, previous=False):
        """ Retrieve current step in execution process

        Parameters
        ----------
        previous : bool, optional
            The step is a backward one (Default: False)

        Returns
        -------
        bool
            Current step status
        """

        try:
            # Remove latest buffer
            if previous:
                del self.__execution_buffer[-1]

            # Add a new buffer to execution process
            elif self.__execution_started:
                output = self.__execution_buffer[-1]["output"]
                flags = self.__execution_buffer[-1]["flags"]
                registres = self.__execution_buffer[-1]["registers"]

                self.__execution_buffer.append({
                    "output": deepcopy(output),
                    "flags": deepcopy(flags),
                    "registers": deepcopy(registres),
                })

            output = self.__execution_buffer[-1]["output"]

            # Reset the previous line status
            if previous and len(output) > 0:
                del output[-1]

            # No program avaible at all...
            if self.program is None:
                return False

            # No section avaible in program
            elif self.__current_section is None:
                return False

            # The program was terminated
            elif self.__execution_stoped:
                return False

            # Avoid to directly switch to step 2 without show step 1
            if not previous and not self.__execution_started:
                self.__execution_step = int()

            # The program has now started
            self.__execution_started = True

            # Retrieve the current instruction from current section
            step = self.__current_section[self.__execution_step]

            if type(step) is AsmInstruction:
                key = step.name.upper()

                if key in self.__instructions_storage:
                    parameters = list()

                    for parameter in step.parameters:

                        if self.has_register(parameter):
                            parameters.append(
                                self.get_register(parameter))

                        elif self.program.has_variable(parameter):
                            parameters.append(
                                self.program.get_variable(parameter))

                        else:
                            try:
                                parameters.append(int(parameter))

                            except ValueError:
                                parameters.append(str(parameter))

                    instruction = self.__instructions_storage[key]

                    result = instruction.run(self, *parameters)

                    # Program has terminated, need to stop execution
                    if result is not None and \
                        result[0] == AsmAction.TerminateProgram:
                        self.__execution_stoped = True

                    output.append((step.position, result))

            else:
                output.append((step.position, None))

        except IndexError as error:
            output.append((None, (AsmAction.TerminateProgram, 0)))

            return False

        return True


    def previous_step(self):
        """ Go to the previous step in execution process

        Returns
        -------
        object or None
            Current step directive
        """

        self.__execution_stoped = False

        self.__execution_step -= 1

        return self.current_step(previous=True)


    def next_step(self):
        """ Go to the next step in execution process

        Returns
        -------
        object or None
            Current step directive
        """

        self.__execution_step += 1

        return self.current_step()


    def can_execute(self):
        """ Check if it's possible to execute program

        Returns
        -------
        bool
            Check result
        """

        return not self.__execution_started and not self.__execution_stoped


    def can_previous_step(self):
        """ Check if it's possible to go anterior in execution process

        Returns
        -------
        bool
            Check result
        """

        section = self.__current_section

        current = self.__execution_step - 1

        if self.__execution_stoped:
            return True

        if self.__execution_started and section is not None and \
            current in range(0, len(section.get()) - 1):
            return True

        return False


    def can_next_step(self):
        """ Check if it's possible to go further in execution process

        Returns
        -------
        bool
            Check result
        """

        if self.__execution_stoped:
            return False

        if not self.__execution_started:
            return True

        section = self.__current_section

        current = self.__execution_step + 1

        if section is not None and current in range(1, len(section.get())):
            return True

        return False


    def reset_execution(self):
        """ Reset execution
        """

        self.__execution_step = int()

        self.__execution_stoped = False
        self.__execution_started = False

        for flag in self.get_flags():
            flag.value = int()

        for register in self.get_registers():
            register.value = int()

        self.__execution_buffer.append({
            "output": list(),
            "flags": deepcopy(self.__flags),
            "registers": deepcopy(self.__registers),
        })


    def save(self, textbuffer):
        """ Save current buffer into file

        Parameters
        ----------
        textbuffer : str
            Text buffer as string
        """

        # Write buffer to file
        with open(self.get_path(), 'w') as pipe:
            pipe.write("%s\n" % textbuffer)


    def has_flag(self, name):
        """ Check if engine has the specified flag

        Parameters
        ----------
        name : str
            Flag name

        Returns
        -------
        bool
            Presence status
        """

        return name in self.__execution_buffer[-1]["flags"].keys()


    def has_register(self, name):
        """ Check if engine has the specified register

        Parameters
        ----------
        name : str
            Register name

        Returns
        -------
        bool
            Presence status
        """

        return name in self.__execution_buffer[-1]["registers"].keys()


    def get_execution_output(self):
        """ Retrieve current execution output

        Returns
        -------
        list
            Output as string list
        """

        return self.__execution_buffer[-1]["output"]


    def get_flag(self, name):
        """ Retrieve a specific flag

        Parameters
        ----------
        name : str
            Flag name

        Returns
        -------
        AsmRegister
            Flag instance

        Raises
        ------
        IndexError
            If name is not available in flags list
        """

        flags = self.__execution_buffer[-1]["flags"]

        if not name in flags.keys():
            raise IndexError("%s key is not available in flags list" % name)

        return flags[name]


    def get_flags(self):
        """ Retrieve flags list

        Returns
        -------
        list
            AsmRegister list
        """

        return list(self.__execution_buffer[-1]["flags"].values())


    def get_register(self, name):
        """ Retrieve a specific register

        Parameters
        ----------
        name : str
            Register name

        Returns
        -------
        AsmRegister
            Register instance

        Raises
        ------
        IndexError
            If name is not available in registers list
        """

        registers = self.__execution_buffer[-1]["registers"]

        if not name in registers.keys():
            raise IndexError("%s key is not available in registers list" % name)

        return registers[name]


    def get_registers(self):
        """ Retrieve registers list

        Returns
        -------
        list
            AsmRegister list
        """

        return list(self.__execution_buffer[-1]["registers"].values())


    def get_registers_arch(self):
        """ Retrieve registers arch list

        Returns
        -------
        list
            Integer list
        """

        return sorted(self.__registers_arch)


    def set_register_value(self, name, value):
        """ Set a new value to a specific register

        Parameters
        ----------
        name : str
            Register name
        value : object
            New value to set

        Raises
        ------
        IndexError
            If name is not available in registers list
        """

        registers = self.__execution_buffer[-1]["registers"]

        if not name in registers.keys():
            raise IndexError("%s key is not available in registers list" % name)

        registers[name].value = value
