# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Enumerate
from enum import Enum

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class AsmError(Enum):

    AlreadyExistsExternal = 0
    AlreadyExistsGlobal = 1

    MissingSectionData = 10
    MissingSectionText = 11
    MissingCloseMacro = 12
    MissingRegister = 13
    MissingVariable = 14

    WrongLabelSyntax = 20
    WrongVariableSyntax = 21
    WrongInstructionSyntax = 22
    WrongExecution = 29

    KernelCallReadError = 30

    InstructionNotImplemented = 99


class AsmAction(Enum):

    SendToOutput = 0
    JumpToLabel = 1

    RetrieveFromInput = 10

    TerminateProgram = 20
