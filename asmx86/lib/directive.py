# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Regex
from re import match

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class AsmInstruction(object):

    def __init__(self, *data, position):
        """ Constructor

        Parameters
        ----------
        data : tuple
            Instruction parameters
        position : int
            Instruction position
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = data[0]

        self.position = position

        self.source = None
        self.destination = None

        if len(data) > 1:
            self.source = data[1]

            if len(data) > 2:
                self.destination = data[2]


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        if self.source is None:
            return self.name

        if self.destination is None:
            return "%s(%s)" % (self.name, str(self.source))

        return "%s(%s, %s)" % (
            self.name, str(self.source), str(self.destination))


    @property
    def parameters(self):
        """ Retrieve instruction available parameters

        Returns
        -------
        list
            Parameters list
        """

        data = list()

        if self.source is not None:
            data.append(self.source)

        if self.destination is not None:
            data.append(self.destination)

        return data


class AsmVariable(object):

    STRUCTURES = {
        "db": 1,
        "dw": 2,
        "dd": 4,
        "df": 6,
        "dq": 8,
        "dt": 10,
    }

    def __init__(self, name, structure, value, position):
        """ Constructor

        Parameters
        ----------
        name : str
            Variable name
        structure : str
            Variable structure
        value : str
            Variable value
        position : int
            Variable position
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = name
        self.position = position

        self.cast(value)

        self.size = int()
        if structure.lower() in self.STRUCTURES.keys():
            self.size = self.STRUCTURES[structure.lower()]


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        return str(self.value)


    def cast(self, value):
        """ Cast value with correct type

        Parameters
        ----------
        value : object
            Value object to cast
        """

        if match(r"^\d+$", str(value)):
            self.value = int(value)

        elif match(r"^\d+\.\d+$", str(value)):
            self.value = float(value)

        else:
            self.value = str(value)


class AsmLabel(object):

    def __init__(self, name, position):
        """ Constructor

        Parameters
        ----------
        name : str
            Label name
        position : int
            Label position
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = name
        self.position = position


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        return self.name


class AsmModule(object):

    def __init__(self, name, position):
        """ Constructor

        Parameters
        ----------
        name : str
            Label name
        position : int
            Label position
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = name
        self.position = position


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        return self.name


class AsmRegister(object):

    def __init__(self, name, size, value=int()):
        """ Constructor

        Parameters
        ----------
        name : str
            Register name
        size : int
            Register size
        value : int, optional
            Register internal value
        """

        # ----------------------------------------
        #   Variables
        # ----------------------------------------

        self.name = name
        self.size = size

        self.cast(value)


    def __str__(self):
        """ Represents current object as string

        Returns
        -------
        str
            Object as string
        """

        return str(self.value)


    def __len__(self):
        """ Retrieve register length

        Returns
        -------
        int
            Register size
        """

        return int(self.size)


    def cast(self, value):
        """ Cast value with correct type

        Parameters
        ----------
        value : object
            Value object to cast
        """

        if match(r"^\d+$", str(value)):
            self.value = int(value)

        elif match(r"^\d+\.\d+$", str(value)):
            self.value = float(value)

        else:
            self.value = str(value)
