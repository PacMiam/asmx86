# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Module - ASMx86
# ------------------------------------------------------------------------------

from asmx86.lib.flag import *

from asmx86.lib.directive import *

# ------------------------------------------------------------------------------
#   Class
# ------------------------------------------------------------------------------

class Instructions(object):

    class ADD(object):

        @staticmethod
        def run(parent, destination, source):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            destination : object
                Register destination
            source : object
                Register source
            """

            if type(destination) is not str:
                destination.value += source


    class CMP(object):

        @staticmethod
        def run(parent, destination, source):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            destination : object
                Register destination
            source : object
                Register source
            """

            return (AsmError.InstructionNotImplemented,)


    class INT(object):

        @staticmethod
        def run(parent, value):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            value : str
                Interrupt call instruction
            """

            try:
                # Check if registers are available
                for register in [ "eax", "ebx", "ecx", "edx" ]:
                    if not parent.has_register(register):
                        return (AsmError.MissingRegister, register)

                # Retrieve register value
                register = parent.get_register("eax")

                # Terminate program
                if int(register.value) == 1:
                    output = parent.get_register("ebx")

                    return (AsmAction.TerminateProgram, output)

                # Write to output
                elif int(register.value) == 4:
                    # If ebx is 1, output is sent to standard output
                    output = parent.get_register("ebx")

                    message = parent.get_register("ecx")
                    len_message = parent.get_register("edx")

                    return (AsmAction.SendToOutput, message)

                elif int(register.value) < 0:
                    return (AsmError.KernelCallReadError,)

            except TypeError as error:
                return (AsmError.WrongExecution,)

            return (AsmError.InstructionNotImplemented,)


    class MOV(object):

        @staticmethod
        def run(parent, destination, source):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            destination : object
                Register destination
            source : object
                Register source
            """

            if type(destination) is not str:
                destination.value = source


    class MUL(object):

        @staticmethod
        def run(parent, destination):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            destination : object
                Register destination
            """

            # Check if registers are available
            if not parent.has_register("eax"):
                return (AsmError.MissingRegister, "eax")

            # Retrieve register value
            register = parent.get_register("eax")

            if type(destination) is not str:
                destination.value *= register.value


    class SUB(object):

        @staticmethod
        def run(parent, destination, source):
            """ Launch instruction

            Parameters
            ----------
            parent : asmx86.lib.engine.Engine
                Engine instance
            destination : object
                Register destination
            source : object
                Register source
            """

            if type(destination) is not str:
                destination.value -= source
