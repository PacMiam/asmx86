# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#   Modules
# ------------------------------------------------------------------------------

# Filesystem
from os.path import exists
from os.path import join as path_join

from glob import glob

# Module
from pkgutil import iter_modules

from importlib import import_module

# System
from sys import exit as sys_exit

from argparse import ArgumentParser

# ------------------------------------------------------------------------------
#   Modules - ASMx86
# ------------------------------------------------------------------------------

from asmx86 import ASMx86

from asmx86.lib.engine import Engine

# ------------------------------------------------------------------------------
#   Launcher
# ------------------------------------------------------------------------------

def check_available_ui():
    """ Retrieve available UI from asmx86.ui module

    Thanks to https://stackoverflow.com/a/1707786

    Returns
    -------
    list
        Found UI as string list
    """

    import asmx86.ui

    ui = list()
    for importer, modname, ispkg in iter_modules(asmx86.ui.__path__):

        if ispkg:
            ui.append(modname)

    return ui


def main():
    """ Main launcher
    """

    parser = ArgumentParser(
        description=ASMx86.Description,
        epilog=ASMx86.Copyleft,
        conflict_handler="resolve")

    parser.add_argument("path", nargs='*', type=str, action="store",
        metavar="FILE", help="the file to open")

    parser.add_argument("-v", "--version", action="version",
        version="%s %s - License %s" % (ASMx86.Name, ASMx86.Version,
        ASMx86.License), help="show the current version")
    parser.add_argument("-d", "--debug",
        action="store_true", help="launch application with debug flag")

    parser_interface = parser.add_argument_group("interface arguments")
    parser_interface.add_argument("--ui", default="gtk", action="store",
        help="set the interface engine (available: %s)" % ', '.join(
        sorted(check_available_ui())))

    args = parser.parse_args()

    # ----------------------------------------
    #   Interface
    # ----------------------------------------

    if args.ui is not None:

        try:
            module = import_module("asmx86.ui.%s.interface" % args.ui)

            module.Interface(args.path, args.debug)

        except AttributeError as error:
            sys_exit("Cannot launch interface: %s" % str(error))

        except ImportError as error:
            sys_exit("Cannot import interface: %s" % str(error))


if __name__ == "__main__":
    main()
