ASMx86
======

ASMx86 est un interpréteur x86 pour faciliter l'apprentissage de l'assembleur
pour les étudiants en informatique.

Licence GPLv3

## Dépendances

 * python3

### Interface GTK+ (optionnel)

 * python3-gobject

### Interface Qt5 (optionnel)

 * python3-sip
 * python3-PyQt5

## Lancement

Pour obtenir les paramètres de lancement de l'application, placez-vous à la
racine du projet et utilisez la commande suivante:

```
python3 -m asmx86 --help
```

Pour lancer l'interface qt, par exemple:

```
python3 -m asmx86 --ui qt
```

## Références

 * http://www.nasm.us/docs.php
 * https://en.wikipedia.org/wiki/X86
 * https://en.wikibooks.org/wiki/X86_Assembly
